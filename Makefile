
.PHONY: testserver
testserver: frontend
	make -C backend/ testserver

.PHONY: frontend
frontend:
	make -C backend/ migrate
	make -C frontend/ build-prod
