package main

import (
	"log"

	"github.com/pocketbase/pocketbase"
	"github.com/pocketbase/pocketbase/core"
)

func registerHooks(app *pocketbase.PocketBase) error {
	// fires for every collection
	app.OnRecordViewRequest().Add(func(e *core.RecordViewEvent) error {
		log.Println("== Record View Request ==")
		log.Println(e.HttpContext)
		log.Println(e.Record)
		return nil
	})

	// fires only for "users" and "articles" collections
	app.OnRecordViewRequest("users", "articles").Add(func(e *core.RecordViewEvent) error {
		log.Println(e.HttpContext)
		log.Println(e.Record)
		return nil
	})

	/*
		// update settings
		app.OnAfterBootstrap().Add(func(e *core.BootstrapEvent) error {
			e.App.RefreshSettings()
			settings := e.App.Settings()

			// force hide controls in production
			settings.Meta.HideControls = !e.App.IsDev()

			encryptionKey := os.Getenv(app.EncryptionEnv())
			return app.Dao().SaveSettings(settings, encryptionKey)
		})
	*/
	return nil
}
