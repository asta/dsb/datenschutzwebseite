package main

import (
	"encoding/json"
	"log"
	"os"

	"github.com/pocketbase/pocketbase"
	"github.com/pocketbase/pocketbase/apis"
	"github.com/pocketbase/pocketbase/core"
	"github.com/pocketbase/pocketbase/plugins/migratecmd"
	"github.com/spf13/cobra"

	// load migrations
	_ "dsb/formulartool/migrations"
)

func main() {
	app := pocketbase.NewWithConfig(pocketbase.Config{
		DefaultDev: false,
	})

	// enable migrations
	migratecmd.MustRegister(app, app.RootCmd, migratecmd.Config{
		// enable auto creation of migration files when making collection changes in the Admin UI
		Automigrate: true,

		TemplateLang: "go",
	})

	// add settings command
	app.RootCmd.AddCommand(buildSettingsCmd(app))

	// serves static files from the provided public dir (if exists)
	app.OnBeforeServe().Add(func(e *core.ServeEvent) error {
		e.Router.GET("/*", apis.StaticDirectoryHandler(
			//os.DirFS("./pb_public"),
			os.DirFS("../frontend/dist"),
			true, // indexFallback
		))
		return nil
	})

	err := registerHooks(app)
	if err != nil {
		log.Fatal(err)
	}

	if err := app.Start(); err != nil {
		log.Fatal(err)
	}
}

func buildSettingsCmd(app *pocketbase.PocketBase) *cobra.Command {
	settingsCmd := &cobra.Command{
		Use:   "settings",
		Short: "Manage application settings",
	}

	exportCmd := &cobra.Command{
		Use:   "export",
		Short: "Export settings to a file",
		Run: func(cmd *cobra.Command, args []string) {
			// get current settings
			app.RefreshSettings()
			settings := app.Settings()

			outputFile, err := cmd.Flags().GetString("output")
			if err != nil {
				log.Fatal(err)
				return
			}

			redact, err := cmd.Flags().GetBool("redact")
			if err != nil {
				log.Fatal(err)
				return
			}

			if redact {
				// Create a deep copy of the settings and redact sensitive fields
				settings, err = settings.RedactClone()
				if err != nil {
					log.Fatal(err)
					return
				}
			}

			data, err := json.MarshalIndent(settings, "", "\t")
			if err != nil {
				log.Fatal(err)
				return
			}

			if outputFile == "-" {
				log.Print(string(data))
			} else {
				err = os.WriteFile(outputFile, data, 0644)
				if err != nil {
					log.Fatal(err)
					return
				}
				log.Printf("Settings exported to %s\n", outputFile)
			}
		},
	}

	importCmd := &cobra.Command{
		Use:   "import",
		Short: "Updates the current settings in the DB with settings from one or more files.",
		Run: func(cmd *cobra.Command, args []string) {
			inputFiles, err := cmd.Flags().GetStringSlice("input")
			if err != nil {
				log.Fatal(err)
				return
			}

			// get current settings
			app.RefreshSettings()
			var settings = app.Settings()

			// merge settings from each file
			for _, inputFile := range inputFiles {
				data, err := os.ReadFile(inputFile)
				if err != nil {
					log.Fatal(err)
					return
				}

				err = json.Unmarshal(data, &settings)
				if err != nil {
					log.Fatal(err)
					return
				}
			}

			// save settings into DB
			encryptionKey := os.Getenv(app.EncryptionEnv())
			err = app.Dao().SaveSettings(settings, encryptionKey)
			if err != nil {
				log.Fatal(err)
				return
			}

			log.Printf("Settings imported from %v\n", inputFiles)
		},
	}

	exportCmd.Flags().BoolP("redact", "r", false, "Redact sensitive fields")
	exportCmd.Flags().StringP("output", "o", "-", "Output file or '-' for stdout")
	importCmd.Flags().StringSliceP("input", "i", []string{"settings.json"}, "Input files (order matters, last file has highest priority)")

	settingsCmd.AddCommand(exportCmd)
	settingsCmd.AddCommand(importCmd)

	return settingsCmd
}
