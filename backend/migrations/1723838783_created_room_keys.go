package migrations

import (
	"encoding/json"

	"github.com/pocketbase/dbx"
	"github.com/pocketbase/pocketbase/daos"
	m "github.com/pocketbase/pocketbase/migrations"
	"github.com/pocketbase/pocketbase/models"
)

func init() {
	m.Register(func(db dbx.Builder) error {
		jsonData := `{
			"id": "0dh9lr15wnx2nt8",
			"created": "2024-08-16 20:06:23.621Z",
			"updated": "2024-08-16 20:06:23.621Z",
			"name": "room_keys",
			"type": "base",
			"system": false,
			"schema": [
				{
					"system": false,
					"id": "9q7pq6p6",
					"name": "room",
					"type": "relation",
					"required": true,
					"presentable": false,
					"unique": false,
					"options": {
						"collectionId": "lti9nh3kp6t799l",
						"cascadeDelete": true,
						"minSelect": null,
						"maxSelect": 1,
						"displayFields": null
					}
				},
				{
					"system": false,
					"id": "tiniobhk",
					"name": "target",
					"type": "relation",
					"required": true,
					"presentable": false,
					"unique": false,
					"options": {
						"collectionId": "0bywlf8vvrhju2j",
						"cascadeDelete": true,
						"minSelect": null,
						"maxSelect": 1,
						"displayFields": null
					}
				},
				{
					"system": false,
					"id": "rpp8148g",
					"name": "source",
					"type": "relation",
					"required": true,
					"presentable": false,
					"unique": false,
					"options": {
						"collectionId": "_pb_users_auth_",
						"cascadeDelete": false,
						"minSelect": null,
						"maxSelect": 1,
						"displayFields": null
					}
				},
				{
					"system": false,
					"id": "xa2ktyce",
					"name": "encrypted_roomkey_b64",
					"type": "text",
					"required": false,
					"presentable": false,
					"unique": false,
					"options": {
						"min": null,
						"max": null,
						"pattern": ""
					}
				}
			],
			"indexes": [
				"CREATE INDEX ` + "`" + `idx_822LMYb` + "`" + ` ON ` + "`" + `room_keys` + "`" + ` (\n  ` + "`" + `room` + "`" + `,\n  ` + "`" + `target` + "`" + `\n)",
				"CREATE INDEX ` + "`" + `idx_cKNFunZ` + "`" + ` ON ` + "`" + `room_keys` + "`" + ` (\n  ` + "`" + `room` + "`" + `,\n  ` + "`" + `source` + "`" + `\n)",
				"CREATE UNIQUE INDEX ` + "`" + `idx_DH9tYtc` + "`" + ` ON ` + "`" + `room_keys` + "`" + ` (\n  ` + "`" + `room` + "`" + `,\n  ` + "`" + `target` + "`" + `,\n  ` + "`" + `source` + "`" + `\n)"
			],
			"listRule": null,
			"viewRule": null,
			"createRule": null,
			"updateRule": null,
			"deleteRule": null,
			"options": {}
		}`

		collection := &models.Collection{}
		if err := json.Unmarshal([]byte(jsonData), &collection); err != nil {
			return err
		}

		return daos.New(db).SaveCollection(collection)
	}, func(db dbx.Builder) error {
		dao := daos.New(db);

		collection, err := dao.FindCollectionByNameOrId("0dh9lr15wnx2nt8")
		if err != nil {
			return err
		}

		return dao.DeleteCollection(collection)
	})
}
