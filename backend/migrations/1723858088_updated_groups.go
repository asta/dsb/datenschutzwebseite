package migrations

import (
	"encoding/json"

	"github.com/pocketbase/dbx"
	"github.com/pocketbase/pocketbase/daos"
	m "github.com/pocketbase/pocketbase/migrations"
	"github.com/pocketbase/pocketbase/models/schema"
)

func init() {
	m.Register(func(db dbx.Builder) error {
		dao := daos.New(db);

		collection, err := dao.FindCollectionByNameOrId("927pit3rmn1umlc")
		if err != nil {
			return err
		}

		if err := json.Unmarshal([]byte(`[
			"CREATE INDEX ` + "`" + `idx_6B64PjL` + "`" + ` ON ` + "`" + `groups` + "`" + ` (` + "`" + `member_users` + "`" + `)"
		]`), &collection.Indexes); err != nil {
			return err
		}

		// remove
		collection.Schema.RemoveField("otcxuhev")

		return dao.SaveCollection(collection)
	}, func(db dbx.Builder) error {
		dao := daos.New(db);

		collection, err := dao.FindCollectionByNameOrId("927pit3rmn1umlc")
		if err != nil {
			return err
		}

		if err := json.Unmarshal([]byte(`[
			"CREATE INDEX ` + "`" + `idx_6B64PjL` + "`" + ` ON ` + "`" + `groups` + "`" + ` (` + "`" + `member_users` + "`" + `)",
			"CREATE INDEX ` + "`" + `idx_9BDhwBj` + "`" + ` ON ` + "`" + `groups` + "`" + ` (` + "`" + `member_groups` + "`" + `)"
		]`), &collection.Indexes); err != nil {
			return err
		}

		// add
		del_member_groups := &schema.SchemaField{}
		if err := json.Unmarshal([]byte(`{
			"system": false,
			"id": "otcxuhev",
			"name": "member_groups",
			"type": "relation",
			"required": false,
			"presentable": false,
			"unique": false,
			"options": {
				"collectionId": "927pit3rmn1umlc",
				"cascadeDelete": false,
				"minSelect": null,
				"maxSelect": null,
				"displayFields": null
			}
		}`), del_member_groups); err != nil {
			return err
		}
		collection.Schema.AddField(del_member_groups)

		return dao.SaveCollection(collection)
	})
}
