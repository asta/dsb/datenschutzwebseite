package migrations

import (
	"encoding/json"

	"github.com/pocketbase/dbx"
	"github.com/pocketbase/pocketbase/daos"
	m "github.com/pocketbase/pocketbase/migrations"
	"github.com/pocketbase/pocketbase/models"
)

func init() {
	m.Register(func(db dbx.Builder) error {
		jsonData := `{
			"id": "9g44jndx55kcooh",
			"created": "2024-08-16 20:06:23.621Z",
			"updated": "2024-08-16 20:06:23.621Z",
			"name": "forms",
			"type": "base",
			"system": false,
			"schema": [
				{
					"system": false,
					"id": "mlsj7a1n",
					"name": "formid",
					"type": "text",
					"required": true,
					"presentable": true,
					"unique": false,
					"options": {
						"min": null,
						"max": null,
						"pattern": ""
					}
				},
				{
					"system": false,
					"id": "nin5xwvc",
					"name": "formversion",
					"type": "number",
					"required": true,
					"presentable": true,
					"unique": false,
					"options": {
						"min": 1,
						"max": null,
						"noDecimal": true
					}
				},
				{
					"system": false,
					"id": "jq4btvrh",
					"name": "title",
					"type": "text",
					"required": true,
					"presentable": true,
					"unique": false,
					"options": {
						"min": null,
						"max": null,
						"pattern": ""
					}
				},
				{
					"system": false,
					"id": "wzbm5evh",
					"name": "description",
					"type": "text",
					"required": true,
					"presentable": false,
					"unique": false,
					"options": {
						"min": null,
						"max": null,
						"pattern": ""
					}
				},
				{
					"system": false,
					"id": "xzdvwmmp",
					"name": "schema",
					"type": "json",
					"required": true,
					"presentable": false,
					"unique": false,
					"options": {
						"maxSize": 2000000
					}
				},
				{
					"system": false,
					"id": "aoaxqrqd",
					"name": "layout",
					"type": "json",
					"required": true,
					"presentable": false,
					"unique": false,
					"options": {
						"maxSize": 2000000
					}
				},
				{
					"system": false,
					"id": "touacyo8",
					"name": "template",
					"type": "text",
					"required": true,
					"presentable": false,
					"unique": false,
					"options": {
						"min": null,
						"max": null,
						"pattern": ""
					}
				},
				{
					"system": false,
					"id": "plkpyid4",
					"name": "recipients",
					"type": "relation",
					"required": true,
					"presentable": false,
					"unique": false,
					"options": {
						"collectionId": "927pit3rmn1umlc",
						"cascadeDelete": false,
						"minSelect": null,
						"maxSelect": null,
						"displayFields": null
					}
				}
			],
			"indexes": [
				"CREATE UNIQUE INDEX ` + "`" + `idx_wV5uoj2` + "`" + ` ON ` + "`" + `forms` + "`" + ` (\n  ` + "`" + `formid` + "`" + `,\n  ` + "`" + `formversion` + "`" + `\n)",
				"CREATE INDEX ` + "`" + `idx_rWL8BQt` + "`" + ` ON ` + "`" + `forms` + "`" + ` (` + "`" + `formid` + "`" + `)"
			],
			"listRule": null,
			"viewRule": null,
			"createRule": null,
			"updateRule": null,
			"deleteRule": null,
			"options": {}
		}`

		collection := &models.Collection{}
		if err := json.Unmarshal([]byte(jsonData), &collection); err != nil {
			return err
		}

		return daos.New(db).SaveCollection(collection)
	}, func(db dbx.Builder) error {
		dao := daos.New(db);

		collection, err := dao.FindCollectionByNameOrId("9g44jndx55kcooh")
		if err != nil {
			return err
		}

		return dao.DeleteCollection(collection)
	})
}
