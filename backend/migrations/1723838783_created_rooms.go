package migrations

import (
	"encoding/json"

	"github.com/pocketbase/dbx"
	"github.com/pocketbase/pocketbase/daos"
	m "github.com/pocketbase/pocketbase/migrations"
	"github.com/pocketbase/pocketbase/models"
)

func init() {
	m.Register(func(db dbx.Builder) error {
		jsonData := `{
			"id": "lti9nh3kp6t799l",
			"created": "2024-08-16 20:06:23.621Z",
			"updated": "2024-08-16 20:06:23.621Z",
			"name": "rooms",
			"type": "base",
			"system": false,
			"schema": [
				{
					"system": false,
					"id": "clvdbdmt",
					"name": "type",
					"type": "select",
					"required": true,
					"presentable": true,
					"unique": false,
					"options": {
						"maxSelect": 1,
						"values": [
							"SUBMISSION",
							"DEFAULTVALUES"
						]
					}
				},
				{
					"system": false,
					"id": "uwkaumkp",
					"name": "subject",
					"type": "text",
					"required": true,
					"presentable": true,
					"unique": false,
					"options": {
						"min": null,
						"max": null,
						"pattern": ""
					}
				},
				{
					"system": false,
					"id": "m1hfdzmk",
					"name": "creator",
					"type": "relation",
					"required": true,
					"presentable": true,
					"unique": false,
					"options": {
						"collectionId": "_pb_users_auth_",
						"cascadeDelete": false,
						"minSelect": null,
						"maxSelect": 1,
						"displayFields": null
					}
				},
				{
					"system": false,
					"id": "cag2cbvq",
					"name": "form",
					"type": "relation",
					"required": true,
					"presentable": false,
					"unique": false,
					"options": {
						"collectionId": "9g44jndx55kcooh",
						"cascadeDelete": false,
						"minSelect": null,
						"maxSelect": 1,
						"displayFields": null
					}
				},
				{
					"system": false,
					"id": "0i2gsrak",
					"name": "member_users",
					"type": "relation",
					"required": false,
					"presentable": false,
					"unique": false,
					"options": {
						"collectionId": "_pb_users_auth_",
						"cascadeDelete": false,
						"minSelect": null,
						"maxSelect": null,
						"displayFields": null
					}
				},
				{
					"system": false,
					"id": "hkphqcb1",
					"name": "member_groups",
					"type": "relation",
					"required": false,
					"presentable": false,
					"unique": false,
					"options": {
						"collectionId": "927pit3rmn1umlc",
						"cascadeDelete": false,
						"minSelect": null,
						"maxSelect": null,
						"displayFields": null
					}
				}
			],
			"indexes": [
				"CREATE INDEX ` + "`" + `idx_YwVQuG5` + "`" + ` ON ` + "`" + `rooms` + "`" + ` (` + "`" + `creator` + "`" + `)",
				"CREATE INDEX ` + "`" + `idx_PewzVKY` + "`" + ` ON ` + "`" + `rooms` + "`" + ` (\n  ` + "`" + `type` + "`" + `,\n  ` + "`" + `form` + "`" + `,\n  ` + "`" + `creator` + "`" + `\n)"
			],
			"listRule": null,
			"viewRule": null,
			"createRule": null,
			"updateRule": null,
			"deleteRule": null,
			"options": {}
		}`

		collection := &models.Collection{}
		if err := json.Unmarshal([]byte(jsonData), &collection); err != nil {
			return err
		}

		return daos.New(db).SaveCollection(collection)
	}, func(db dbx.Builder) error {
		dao := daos.New(db);

		collection, err := dao.FindCollectionByNameOrId("lti9nh3kp6t799l")
		if err != nil {
			return err
		}

		return dao.DeleteCollection(collection)
	})
}
