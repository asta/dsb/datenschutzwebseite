package migrations

import (
	"encoding/json"

	"github.com/pocketbase/dbx"
	"github.com/pocketbase/pocketbase/daos"
	m "github.com/pocketbase/pocketbase/migrations"
)

func init() {
	m.Register(func(db dbx.Builder) error {
		dao := daos.New(db);

		collection, err := dao.FindCollectionByNameOrId("0bywlf8vvrhju2j")
		if err != nil {
			return err
		}

		if err := json.Unmarshal([]byte(`[
			"CREATE UNIQUE INDEX ` + "`" + `idx_EsG1sO8` + "`" + ` ON ` + "`" + `user_keys` + "`" + ` (` + "`" + `fingerprint` + "`" + `)",
			"CREATE INDEX ` + "`" + `idx_Ij1U7FX` + "`" + ` ON ` + "`" + `user_keys` + "`" + ` (` + "`" + `user` + "`" + `)"
		]`), &collection.Indexes); err != nil {
			return err
		}

		return dao.SaveCollection(collection)
	}, func(db dbx.Builder) error {
		dao := daos.New(db);

		collection, err := dao.FindCollectionByNameOrId("0bywlf8vvrhju2j")
		if err != nil {
			return err
		}

		if err := json.Unmarshal([]byte(`[
			"CREATE UNIQUE INDEX ` + "`" + `idx_EsG1sO8` + "`" + ` ON ` + "`" + `user_keys` + "`" + ` (` + "`" + `fingerprint` + "`" + `)"
		]`), &collection.Indexes); err != nil {
			return err
		}

		return dao.SaveCollection(collection)
	})
}
