package migrations

import (
	"encoding/json"

	"github.com/pocketbase/dbx"
	"github.com/pocketbase/pocketbase/daos"
	m "github.com/pocketbase/pocketbase/migrations"
	"github.com/pocketbase/pocketbase/models/schema"
)

func init() {
	m.Register(func(db dbx.Builder) error {
		dao := daos.New(db);

		collection, err := dao.FindCollectionByNameOrId("0bywlf8vvrhju2j")
		if err != nil {
			return err
		}

		if err := json.Unmarshal([]byte(`[
			"CREATE UNIQUE INDEX ` + "`" + `idx_EsG1sO8` + "`" + ` ON ` + "`" + `user_keys` + "`" + ` (` + "`" + `fingerprint` + "`" + `)"
		]`), &collection.Indexes); err != nil {
			return err
		}

		// add
		new_fingerprint := &schema.SchemaField{}
		if err := json.Unmarshal([]byte(`{
			"system": false,
			"id": "c8vrzxkn",
			"name": "fingerprint",
			"type": "text",
			"required": true,
			"presentable": true,
			"unique": false,
			"options": {
				"min": null,
				"max": null,
				"pattern": ""
			}
		}`), new_fingerprint); err != nil {
			return err
		}
		collection.Schema.AddField(new_fingerprint)

		// update
		edit_encryption_key_b64 := &schema.SchemaField{}
		if err := json.Unmarshal([]byte(`{
			"system": false,
			"id": "x9xoiz7e",
			"name": "encryption_key_b64",
			"type": "text",
			"required": true,
			"presentable": false,
			"unique": false,
			"options": {
				"min": null,
				"max": null,
				"pattern": ""
			}
		}`), edit_encryption_key_b64); err != nil {
			return err
		}
		collection.Schema.AddField(edit_encryption_key_b64)

		// update
		edit_decryption_key_b64_encrypted := &schema.SchemaField{}
		if err := json.Unmarshal([]byte(`{
			"system": false,
			"id": "kzm3kzju",
			"name": "decryption_key_b64_encrypted",
			"type": "json",
			"required": true,
			"presentable": false,
			"unique": false,
			"options": {
				"maxSize": 2000000
			}
		}`), edit_decryption_key_b64_encrypted); err != nil {
			return err
		}
		collection.Schema.AddField(edit_decryption_key_b64_encrypted)

		// update
		edit_verify_key_b64 := &schema.SchemaField{}
		if err := json.Unmarshal([]byte(`{
			"system": false,
			"id": "vef0s3rw",
			"name": "verify_key_b64",
			"type": "text",
			"required": true,
			"presentable": false,
			"unique": false,
			"options": {
				"min": null,
				"max": null,
				"pattern": ""
			}
		}`), edit_verify_key_b64); err != nil {
			return err
		}
		collection.Schema.AddField(edit_verify_key_b64)

		// update
		edit_sign_key_b64_encrypted := &schema.SchemaField{}
		if err := json.Unmarshal([]byte(`{
			"system": false,
			"id": "xrgpz5lz",
			"name": "sign_key_b64_encrypted",
			"type": "json",
			"required": true,
			"presentable": false,
			"unique": false,
			"options": {
				"maxSize": 2000000
			}
		}`), edit_sign_key_b64_encrypted); err != nil {
			return err
		}
		collection.Schema.AddField(edit_sign_key_b64_encrypted)

		// update
		edit_encryption_key_signature_b64 := &schema.SchemaField{}
		if err := json.Unmarshal([]byte(`{
			"system": false,
			"id": "dgm2be1m",
			"name": "encryption_key_signature_b64",
			"type": "text",
			"required": true,
			"presentable": false,
			"unique": false,
			"options": {
				"min": null,
				"max": null,
				"pattern": ""
			}
		}`), edit_encryption_key_signature_b64); err != nil {
			return err
		}
		collection.Schema.AddField(edit_encryption_key_signature_b64)

		return dao.SaveCollection(collection)
	}, func(db dbx.Builder) error {
		dao := daos.New(db);

		collection, err := dao.FindCollectionByNameOrId("0bywlf8vvrhju2j")
		if err != nil {
			return err
		}

		if err := json.Unmarshal([]byte(`[]`), &collection.Indexes); err != nil {
			return err
		}

		// remove
		collection.Schema.RemoveField("c8vrzxkn")

		// update
		edit_encryption_key_b64 := &schema.SchemaField{}
		if err := json.Unmarshal([]byte(`{
			"system": false,
			"id": "x9xoiz7e",
			"name": "encryptionkey_publickey_b64",
			"type": "text",
			"required": true,
			"presentable": false,
			"unique": false,
			"options": {
				"min": null,
				"max": null,
				"pattern": ""
			}
		}`), edit_encryption_key_b64); err != nil {
			return err
		}
		collection.Schema.AddField(edit_encryption_key_b64)

		// update
		edit_decryption_key_b64_encrypted := &schema.SchemaField{}
		if err := json.Unmarshal([]byte(`{
			"system": false,
			"id": "kzm3kzju",
			"name": "encryptionkey_encrypted_privatekey",
			"type": "json",
			"required": true,
			"presentable": false,
			"unique": false,
			"options": {
				"maxSize": 2000000
			}
		}`), edit_decryption_key_b64_encrypted); err != nil {
			return err
		}
		collection.Schema.AddField(edit_decryption_key_b64_encrypted)

		// update
		edit_verify_key_b64 := &schema.SchemaField{}
		if err := json.Unmarshal([]byte(`{
			"system": false,
			"id": "vef0s3rw",
			"name": "signaturekey_publickey_b64",
			"type": "text",
			"required": true,
			"presentable": false,
			"unique": false,
			"options": {
				"min": null,
				"max": null,
				"pattern": ""
			}
		}`), edit_verify_key_b64); err != nil {
			return err
		}
		collection.Schema.AddField(edit_verify_key_b64)

		// update
		edit_sign_key_b64_encrypted := &schema.SchemaField{}
		if err := json.Unmarshal([]byte(`{
			"system": false,
			"id": "xrgpz5lz",
			"name": "signaturekey_encrypted_privatekey",
			"type": "json",
			"required": true,
			"presentable": false,
			"unique": false,
			"options": {
				"maxSize": 2000000
			}
		}`), edit_sign_key_b64_encrypted); err != nil {
			return err
		}
		collection.Schema.AddField(edit_sign_key_b64_encrypted)

		// update
		edit_encryption_key_signature_b64 := &schema.SchemaField{}
		if err := json.Unmarshal([]byte(`{
			"system": false,
			"id": "dgm2be1m",
			"name": "encryptionkey_publickey_signature_b64",
			"type": "text",
			"required": true,
			"presentable": false,
			"unique": false,
			"options": {
				"min": null,
				"max": null,
				"pattern": ""
			}
		}`), edit_encryption_key_signature_b64); err != nil {
			return err
		}
		collection.Schema.AddField(edit_encryption_key_signature_b64)

		return dao.SaveCollection(collection)
	})
}
