package migrations

import (
	"encoding/json"

	"github.com/pocketbase/dbx"
	"github.com/pocketbase/pocketbase/daos"
	m "github.com/pocketbase/pocketbase/migrations"
	"github.com/pocketbase/pocketbase/models/schema"
)

func init() {
	m.Register(func(db dbx.Builder) error {
		dao := daos.New(db);

		collection, err := dao.FindCollectionByNameOrId("0dh9lr15wnx2nt8")
		if err != nil {
			return err
		}

		// remove
		collection.Schema.RemoveField("xa2ktyce")

		// add
		new_roomkey_encrypted := &schema.SchemaField{}
		if err := json.Unmarshal([]byte(`{
			"system": false,
			"id": "kxutfqnd",
			"name": "roomkey_encrypted",
			"type": "json",
			"required": true,
			"presentable": false,
			"unique": false,
			"options": {
				"maxSize": 2000000
			}
		}`), new_roomkey_encrypted); err != nil {
			return err
		}
		collection.Schema.AddField(new_roomkey_encrypted)

		return dao.SaveCollection(collection)
	}, func(db dbx.Builder) error {
		dao := daos.New(db);

		collection, err := dao.FindCollectionByNameOrId("0dh9lr15wnx2nt8")
		if err != nil {
			return err
		}

		// add
		del_encrypted_roomkey_b64 := &schema.SchemaField{}
		if err := json.Unmarshal([]byte(`{
			"system": false,
			"id": "xa2ktyce",
			"name": "encrypted_roomkey_b64",
			"type": "text",
			"required": false,
			"presentable": false,
			"unique": false,
			"options": {
				"min": null,
				"max": null,
				"pattern": ""
			}
		}`), del_encrypted_roomkey_b64); err != nil {
			return err
		}
		collection.Schema.AddField(del_encrypted_roomkey_b64)

		// remove
		collection.Schema.RemoveField("kxutfqnd")

		return dao.SaveCollection(collection)
	})
}
