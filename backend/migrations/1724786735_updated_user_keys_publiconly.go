package migrations

import (
	"encoding/json"

	"github.com/pocketbase/dbx"
	"github.com/pocketbase/pocketbase/daos"
	m "github.com/pocketbase/pocketbase/migrations"
	"github.com/pocketbase/pocketbase/models/schema"
)

func init() {
	m.Register(func(db dbx.Builder) error {
		dao := daos.New(db);

		collection, err := dao.FindCollectionByNameOrId("wdxabydvxcxiyy6")
		if err != nil {
			return err
		}

		options := map[string]any{}
		if err := json.Unmarshal([]byte(`{
			"query": "SELECT id, user, fingerprint, encryption_key_b64, verify_key_b64, encryption_key_signature_b64 FROM user_keys"
		}`), &options); err != nil {
			return err
		}
		collection.SetOptions(options)

		// remove
		collection.Schema.RemoveField("c1ymzear")

		// remove
		collection.Schema.RemoveField("1sq0kmxj")

		// remove
		collection.Schema.RemoveField("qktx8uhe")

		// remove
		collection.Schema.RemoveField("btcgcswa")

		// add
		new_user := &schema.SchemaField{}
		if err := json.Unmarshal([]byte(`{
			"system": false,
			"id": "aviz51vr",
			"name": "user",
			"type": "relation",
			"required": true,
			"presentable": false,
			"unique": false,
			"options": {
				"collectionId": "_pb_users_auth_",
				"cascadeDelete": true,
				"minSelect": null,
				"maxSelect": 1,
				"displayFields": null
			}
		}`), new_user); err != nil {
			return err
		}
		collection.Schema.AddField(new_user)

		// add
		new_fingerprint := &schema.SchemaField{}
		if err := json.Unmarshal([]byte(`{
			"system": false,
			"id": "tk9ckrzd",
			"name": "fingerprint",
			"type": "text",
			"required": true,
			"presentable": true,
			"unique": false,
			"options": {
				"min": null,
				"max": null,
				"pattern": ""
			}
		}`), new_fingerprint); err != nil {
			return err
		}
		collection.Schema.AddField(new_fingerprint)

		// add
		new_encryption_key_b64 := &schema.SchemaField{}
		if err := json.Unmarshal([]byte(`{
			"system": false,
			"id": "7phcej7a",
			"name": "encryption_key_b64",
			"type": "text",
			"required": true,
			"presentable": false,
			"unique": false,
			"options": {
				"min": null,
				"max": null,
				"pattern": ""
			}
		}`), new_encryption_key_b64); err != nil {
			return err
		}
		collection.Schema.AddField(new_encryption_key_b64)

		// add
		new_verify_key_b64 := &schema.SchemaField{}
		if err := json.Unmarshal([]byte(`{
			"system": false,
			"id": "mdirvjl9",
			"name": "verify_key_b64",
			"type": "text",
			"required": true,
			"presentable": false,
			"unique": false,
			"options": {
				"min": null,
				"max": null,
				"pattern": ""
			}
		}`), new_verify_key_b64); err != nil {
			return err
		}
		collection.Schema.AddField(new_verify_key_b64)

		// add
		new_encryption_key_signature_b64 := &schema.SchemaField{}
		if err := json.Unmarshal([]byte(`{
			"system": false,
			"id": "k65khc9u",
			"name": "encryption_key_signature_b64",
			"type": "text",
			"required": true,
			"presentable": false,
			"unique": false,
			"options": {
				"min": null,
				"max": null,
				"pattern": ""
			}
		}`), new_encryption_key_signature_b64); err != nil {
			return err
		}
		collection.Schema.AddField(new_encryption_key_signature_b64)

		return dao.SaveCollection(collection)
	}, func(db dbx.Builder) error {
		dao := daos.New(db);

		collection, err := dao.FindCollectionByNameOrId("wdxabydvxcxiyy6")
		if err != nil {
			return err
		}

		options := map[string]any{}
		if err := json.Unmarshal([]byte(`{
			"query": "SELECT id, user, encryptionkey_publickey_b64, signaturekey_publickey_b64, encryptionkey_publickey_signature_b64 FROM user_keys"
		}`), &options); err != nil {
			return err
		}
		collection.SetOptions(options)

		// add
		del_user := &schema.SchemaField{}
		if err := json.Unmarshal([]byte(`{
			"system": false,
			"id": "c1ymzear",
			"name": "user",
			"type": "relation",
			"required": true,
			"presentable": false,
			"unique": false,
			"options": {
				"collectionId": "_pb_users_auth_",
				"cascadeDelete": true,
				"minSelect": null,
				"maxSelect": 1,
				"displayFields": null
			}
		}`), del_user); err != nil {
			return err
		}
		collection.Schema.AddField(del_user)

		// add
		del_encryptionkey_publickey_b64 := &schema.SchemaField{}
		if err := json.Unmarshal([]byte(`{
			"system": false,
			"id": "1sq0kmxj",
			"name": "encryptionkey_publickey_b64",
			"type": "text",
			"required": true,
			"presentable": false,
			"unique": false,
			"options": {
				"min": null,
				"max": null,
				"pattern": ""
			}
		}`), del_encryptionkey_publickey_b64); err != nil {
			return err
		}
		collection.Schema.AddField(del_encryptionkey_publickey_b64)

		// add
		del_signaturekey_publickey_b64 := &schema.SchemaField{}
		if err := json.Unmarshal([]byte(`{
			"system": false,
			"id": "qktx8uhe",
			"name": "signaturekey_publickey_b64",
			"type": "text",
			"required": true,
			"presentable": false,
			"unique": false,
			"options": {
				"min": null,
				"max": null,
				"pattern": ""
			}
		}`), del_signaturekey_publickey_b64); err != nil {
			return err
		}
		collection.Schema.AddField(del_signaturekey_publickey_b64)

		// add
		del_encryptionkey_publickey_signature_b64 := &schema.SchemaField{}
		if err := json.Unmarshal([]byte(`{
			"system": false,
			"id": "btcgcswa",
			"name": "encryptionkey_publickey_signature_b64",
			"type": "text",
			"required": true,
			"presentable": false,
			"unique": false,
			"options": {
				"min": null,
				"max": null,
				"pattern": ""
			}
		}`), del_encryptionkey_publickey_signature_b64); err != nil {
			return err
		}
		collection.Schema.AddField(del_encryptionkey_publickey_signature_b64)

		// remove
		collection.Schema.RemoveField("aviz51vr")

		// remove
		collection.Schema.RemoveField("tk9ckrzd")

		// remove
		collection.Schema.RemoveField("7phcej7a")

		// remove
		collection.Schema.RemoveField("mdirvjl9")

		// remove
		collection.Schema.RemoveField("k65khc9u")

		return dao.SaveCollection(collection)
	})
}
