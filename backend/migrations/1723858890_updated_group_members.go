package migrations

import (
	"encoding/json"

	"github.com/pocketbase/dbx"
	"github.com/pocketbase/pocketbase/daos"
	m "github.com/pocketbase/pocketbase/migrations"
	"github.com/pocketbase/pocketbase/models/schema"
)

func init() {
	m.Register(func(db dbx.Builder) error {
		dao := daos.New(db);

		collection, err := dao.FindCollectionByNameOrId("wuium6ixqvebklh")
		if err != nil {
			return err
		}

		// add
		new_reason := &schema.SchemaField{}
		if err := json.Unmarshal([]byte(`{
			"system": false,
			"id": "thwmjzpi",
			"name": "reason",
			"type": "json",
			"required": false,
			"presentable": false,
			"unique": false,
			"options": {
				"maxSize": 2000000
			}
		}`), new_reason); err != nil {
			return err
		}
		collection.Schema.AddField(new_reason)

		return dao.SaveCollection(collection)
	}, func(db dbx.Builder) error {
		dao := daos.New(db);

		collection, err := dao.FindCollectionByNameOrId("wuium6ixqvebklh")
		if err != nil {
			return err
		}

		// remove
		collection.Schema.RemoveField("thwmjzpi")

		return dao.SaveCollection(collection)
	})
}
