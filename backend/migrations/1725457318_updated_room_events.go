package migrations

import (
	"encoding/json"

	"github.com/pocketbase/dbx"
	"github.com/pocketbase/pocketbase/daos"
	m "github.com/pocketbase/pocketbase/migrations"
	"github.com/pocketbase/pocketbase/models/schema"
	"github.com/pocketbase/pocketbase/tools/types"
)

func init() {
	m.Register(func(db dbx.Builder) error {
		dao := daos.New(db);

		collection, err := dao.FindCollectionByNameOrId("7jnct633o1gdrxr")
		if err != nil {
			return err
		}

		collection.ListRule = types.Pointer("")

		collection.ViewRule = types.Pointer("")

		collection.CreateRule = types.Pointer("")

		// remove
		collection.Schema.RemoveField("qfc2ytav")

		// remove
		collection.Schema.RemoveField("nrikhzqg")

		// remove
		collection.Schema.RemoveField("bkucjjrg")

		return dao.SaveCollection(collection)
	}, func(db dbx.Builder) error {
		dao := daos.New(db);

		collection, err := dao.FindCollectionByNameOrId("7jnct633o1gdrxr")
		if err != nil {
			return err
		}

		collection.ListRule = nil

		collection.ViewRule = nil

		collection.CreateRule = nil

		// add
		del_event_plaintext := &schema.SchemaField{}
		if err := json.Unmarshal([]byte(`{
			"system": false,
			"id": "qfc2ytav",
			"name": "event_plaintext",
			"type": "json",
			"required": false,
			"presentable": false,
			"unique": false,
			"options": {
				"maxSize": 2000000
			}
		}`), del_event_plaintext); err != nil {
			return err
		}
		collection.Schema.AddField(del_event_plaintext)

		// add
		del_signature_b64 := &schema.SchemaField{}
		if err := json.Unmarshal([]byte(`{
			"system": false,
			"id": "nrikhzqg",
			"name": "signature_b64",
			"type": "text",
			"required": false,
			"presentable": false,
			"unique": false,
			"options": {
				"min": null,
				"max": null,
				"pattern": ""
			}
		}`), del_signature_b64); err != nil {
			return err
		}
		collection.Schema.AddField(del_signature_b64)

		// add
		del_signature_key := &schema.SchemaField{}
		if err := json.Unmarshal([]byte(`{
			"system": false,
			"id": "bkucjjrg",
			"name": "signature_key",
			"type": "relation",
			"required": false,
			"presentable": false,
			"unique": false,
			"options": {
				"collectionId": "0bywlf8vvrhju2j",
				"cascadeDelete": false,
				"minSelect": null,
				"maxSelect": 1,
				"displayFields": null
			}
		}`), del_signature_key); err != nil {
			return err
		}
		collection.Schema.AddField(del_signature_key)

		return dao.SaveCollection(collection)
	})
}
