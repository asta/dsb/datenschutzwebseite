package migrations

import (
	"encoding/json"

	"github.com/pocketbase/dbx"
	"github.com/pocketbase/pocketbase/daos"
	m "github.com/pocketbase/pocketbase/migrations"
	"github.com/pocketbase/pocketbase/models"
)

func init() {
	m.Register(func(db dbx.Builder) error {
		jsonData := `{
			"id": "7jnct633o1gdrxr",
			"created": "2024-08-16 20:06:23.621Z",
			"updated": "2024-08-16 20:06:23.621Z",
			"name": "room_events",
			"type": "base",
			"system": false,
			"schema": [
				{
					"system": false,
					"id": "agsrbj5k",
					"name": "room",
					"type": "relation",
					"required": true,
					"presentable": false,
					"unique": false,
					"options": {
						"collectionId": "lti9nh3kp6t799l",
						"cascadeDelete": true,
						"minSelect": null,
						"maxSelect": 1,
						"displayFields": null
					}
				},
				{
					"system": false,
					"id": "eya4ubvq",
					"name": "type",
					"type": "select",
					"required": true,
					"presentable": false,
					"unique": false,
					"options": {
						"maxSelect": 1,
						"values": [
							"STATUS_CHANGE",
							"MEMBER_ADD",
							"MESSAGE",
							"SUBMISSIONVALUE"
						]
					}
				},
				{
					"system": false,
					"id": "ch5pzsqe",
					"name": "author",
					"type": "relation",
					"required": true,
					"presentable": false,
					"unique": false,
					"options": {
						"collectionId": "_pb_users_auth_",
						"cascadeDelete": false,
						"minSelect": null,
						"maxSelect": 1,
						"displayFields": null
					}
				},
				{
					"system": false,
					"id": "wzrmzm2j",
					"name": "event_encrypted",
					"type": "json",
					"required": true,
					"presentable": false,
					"unique": false,
					"options": {
						"maxSize": 2000000
					}
				},
				{
					"system": false,
					"id": "qfc2ytav",
					"name": "event_plaintext",
					"type": "json",
					"required": false,
					"presentable": false,
					"unique": false,
					"options": {
						"maxSize": 2000000
					}
				},
				{
					"system": false,
					"id": "nrikhzqg",
					"name": "signature_b64",
					"type": "text",
					"required": false,
					"presentable": false,
					"unique": false,
					"options": {
						"min": null,
						"max": null,
						"pattern": ""
					}
				},
				{
					"system": false,
					"id": "bkucjjrg",
					"name": "signature_key",
					"type": "relation",
					"required": false,
					"presentable": false,
					"unique": false,
					"options": {
						"collectionId": "0bywlf8vvrhju2j",
						"cascadeDelete": false,
						"minSelect": null,
						"maxSelect": 1,
						"displayFields": null
					}
				}
			],
			"indexes": [
				"CREATE INDEX ` + "`" + `idx_zA8kYLD` + "`" + ` ON ` + "`" + `room_events` + "`" + ` (\n  ` + "`" + `room` + "`" + `,\n  ` + "`" + `created` + "`" + `\n)",
				"CREATE INDEX ` + "`" + `idx_bhMfznZ` + "`" + ` ON ` + "`" + `room_events` + "`" + ` (\n  ` + "`" + `room` + "`" + `,\n  ` + "`" + `type` + "`" + `\n)"
			],
			"listRule": null,
			"viewRule": null,
			"createRule": null,
			"updateRule": null,
			"deleteRule": null,
			"options": {}
		}`

		collection := &models.Collection{}
		if err := json.Unmarshal([]byte(jsonData), &collection); err != nil {
			return err
		}

		return daos.New(db).SaveCollection(collection)
	}, func(db dbx.Builder) error {
		dao := daos.New(db);

		collection, err := dao.FindCollectionByNameOrId("7jnct633o1gdrxr")
		if err != nil {
			return err
		}

		return dao.DeleteCollection(collection)
	})
}
