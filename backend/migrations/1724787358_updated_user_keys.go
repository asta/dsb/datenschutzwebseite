package migrations

import (
	"encoding/json"

	"github.com/pocketbase/dbx"
	"github.com/pocketbase/pocketbase/daos"
	m "github.com/pocketbase/pocketbase/migrations"
	"github.com/pocketbase/pocketbase/models/schema"
)

func init() {
	m.Register(func(db dbx.Builder) error {
		dao := daos.New(db);

		collection, err := dao.FindCollectionByNameOrId("0bywlf8vvrhju2j")
		if err != nil {
			return err
		}

		// update
		edit_verification_key_b64 := &schema.SchemaField{}
		if err := json.Unmarshal([]byte(`{
			"system": false,
			"id": "vef0s3rw",
			"name": "verification_key_b64",
			"type": "text",
			"required": true,
			"presentable": false,
			"unique": false,
			"options": {
				"min": null,
				"max": null,
				"pattern": ""
			}
		}`), edit_verification_key_b64); err != nil {
			return err
		}
		collection.Schema.AddField(edit_verification_key_b64)

		// update
		edit_signature_key_b64_encrypted := &schema.SchemaField{}
		if err := json.Unmarshal([]byte(`{
			"system": false,
			"id": "xrgpz5lz",
			"name": "signature_key_b64_encrypted",
			"type": "json",
			"required": true,
			"presentable": false,
			"unique": false,
			"options": {
				"maxSize": 2000000
			}
		}`), edit_signature_key_b64_encrypted); err != nil {
			return err
		}
		collection.Schema.AddField(edit_signature_key_b64_encrypted)

		return dao.SaveCollection(collection)
	}, func(db dbx.Builder) error {
		dao := daos.New(db);

		collection, err := dao.FindCollectionByNameOrId("0bywlf8vvrhju2j")
		if err != nil {
			return err
		}

		// update
		edit_verification_key_b64 := &schema.SchemaField{}
		if err := json.Unmarshal([]byte(`{
			"system": false,
			"id": "vef0s3rw",
			"name": "verify_key_b64",
			"type": "text",
			"required": true,
			"presentable": false,
			"unique": false,
			"options": {
				"min": null,
				"max": null,
				"pattern": ""
			}
		}`), edit_verification_key_b64); err != nil {
			return err
		}
		collection.Schema.AddField(edit_verification_key_b64)

		// update
		edit_signature_key_b64_encrypted := &schema.SchemaField{}
		if err := json.Unmarshal([]byte(`{
			"system": false,
			"id": "xrgpz5lz",
			"name": "sign_key_b64_encrypted",
			"type": "json",
			"required": true,
			"presentable": false,
			"unique": false,
			"options": {
				"maxSize": 2000000
			}
		}`), edit_signature_key_b64_encrypted); err != nil {
			return err
		}
		collection.Schema.AddField(edit_signature_key_b64_encrypted)

		return dao.SaveCollection(collection)
	})
}
