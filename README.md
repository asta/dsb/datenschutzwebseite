---
lang: de
---

# Formulartool

## Einrichtungsschritte im Adminpanel

1. Unter `Settings > Application` Application Name zu "Formulartool" setzen und in Prod `Hide collection create and edit controls` aktivieren.
1. Unter `Settings > Mail settings` E-Mailkonto einstellen.
    - TODO
1. Unter `Settings > Auth` providers den GitLab Provider einrichten.
    - Client ID und Secret in Testinstanzen: https://gitlab.gwdg.de/groups/asta/dsb/-/settings/applications/118
    - Client ID und Secret in Prod: https://gitlab.gwdg.de/groups/asta/dsb/-/settings/applications/94
    - Auth URL: https://gitlab.gwdg.de/oauth/authorize
    - Token URL: https://gitlab.gwdg.de/oauth/token
    - User API Url: https://gitlab.gwdg.de/api/v4/user
1. Unter `Settings > Backups > Backups options` automatische Backups einrichten.
    - TODO
1. Unter `Logs > Logs settings` logs konfigurieren.
    - TODO


## Links

- https://pocketbase.io/
- https://github.com/benallfree/awesome-pocketbase
