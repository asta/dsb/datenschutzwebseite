#!/bin/bash

# Exit on error
set -e

pushd "$(dirname "$0")"
echo "Installing Requirements..."
apt-get install -y bash make nginx


wget -O go.tar.gz "https://go.dev/dl/go1.23.0.linux-amd64.tar.gz"
rm -rf /usr/local/go
tar -C /usr/local -xzf go.tar.gz
echo "export PATH=\"\$PATH:/usr/local/go/bin\"" > /etc/profile.d/go.sh


#echo "Setting things up..."
#./setup.sh

echo "Configurating Nginx..."
cp -v test.nginx.conf /etc/nginx/sites-available/dsbwebsite
ln -vfs /etc/nginx/sites-available/dsbwebsite /etc/nginx/sites-enabled/dsbwebsite
rm -vf /etc/nginx/sites-enabled/default
systemctl restart nginx

echo "Systemd..."
cp -v ./dsbwebsite.service /etc/systemd/system/dsbwebsite.service
systemctl daemon-reload
#systemctl start dsbwebsite
#systemctl enable dsbwebsite

popd

