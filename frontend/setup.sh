#!/bin/bash

# Exit on error
set -e

pushd "$(dirname "$0")" >/dev/null

git submodule init
git submodule update

mkdir -p deps

# Install nodejs
NODEJSVERSION="v20.16.0"
NODEJSTYPE="linux-x64"
pushd "./deps" >/dev/null
	if [ -x "./bin/node" ] && [ "$(./bin/node --version)" = "${NODEJSVERSION}" ]; then
		echo "Nodejs is already ${NODEJSVERSION}"
	else
		echo "Installing Nodejs ${NODEJSVERSION}"
		wget -O "node.tar.xz" "https://nodejs.org/dist/${NODEJSVERSION}/node-${NODEJSVERSION}-${NODEJSTYPE}.tar.xz"
		tar xf "node.tar.xz"
		rm -rf "node.tar.xz"
		cp -rf --update ./node-${NODEJSVERSION}-${NODEJSTYPE}/* .
		rm -rf ./node-${NODEJSVERSION}-${NODEJSTYPE}/
	fi
popd >/dev/null
echo "export PATH=\"\$(realpath \"./deps/bin\"):\$PATH\""
PATH="$(realpath "./deps/bin"):$PATH"
export PATH

# Disable exit on error so sourcing this file doesn't cause weird issues
set +e

popd >/dev/null

