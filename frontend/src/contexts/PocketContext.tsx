// based on https://dev.to/franciscomendes10866/how-to-use-pocketbase-authentication-with-react-context-11be
import type { ReactNode } from 'react'
import {
	createContext,
	useContext,
	useCallback,
	useState,
	useEffect,
	useMemo,
} from "react";
import PocketBase from "pocketbase";
import type { PocketContextType } from '@/types/pocket';
import type { TypedPocketBase, UsersResponse } from "@/types/pocketbase-types"
import { API, User } from '@/lib/api';
//import { useInterval } from "usehooks-ts";
//import jwtDecode from "jwt-decode";
//import ms from "ms";

const BASE_URL = import.meta.env.PROD ? "/" : "http://127.0.0.1:8090";
//const fiveMinutesInMs = ms("5 minutes");
//const twoMinutesInMs = ms("2 minutes");

const PocketContext = createContext<PocketContextType | null>(null);

export function PocketProvider({ children }: { children: ReactNode }) {
	const pb = useMemo(() => new PocketBase(BASE_URL) as TypedPocketBase, []);
	pb.autoCancellation(false); // TODO this is a hack

	const [token, setToken] = useState(pb.authStore.token);
	const [user, setUser] = useState(undefined as User | undefined);

	const api = useMemo(() => new API(pb), [pb]);
	api.setCurrentUser(user); // react will automatically call this again when setUser is called!
	//useEffect(() => {
	//	// user retains it's state property
	//	api.setCurrentUser(user);
	//}, [user]);

	useEffect(() => {
		pb.authStore.onChange((token, model) => {
			setToken(token);
			//console.log(model)
			if (model) {
				// user logged in
				setUser(new User(api, model as UsersResponse));
			} else {
				// user logged out
				setUser(undefined);
			}
		});
	}, [pb.authStore, api]);

	//const register = useCallback(async (email, password) => {
	//	return await pb
	//		.collection("users")
	//		.create({ email, password, passwordConfirm: password });
	//}, []);

	const login = useCallback(async (provider = "gitlab") => {
		// do the authenticating
		await pb.collection('users').authWithOAuth2({ provider: provider });

		// very ugly busy waiting for setUser to be called in the useEffect above and thus api.currentuser being updated.
		while (!api.currentuser) {
			await new Promise((resolve) => { setTimeout(resolve, 10) });
		}
		return api.currentuser;
	}, [pb, api]);

	const logout = useCallback(() => {
		pb.authStore.clear();
	}, [pb.authStore]);

	// TODO refresh Sessions
	//const refreshSession = useCallback(async () => {
	//	if (!pb.authStore.isValid) return;
	//	const decoded = jwtDecode(token);
	//	const tokenExpiration = decoded.exp;
	//	const expirationWithBuffer = (decoded.exp + fiveMinutesInMs) / 1000;
	//	if (tokenExpiration < expirationWithBuffer) {
	//		await pb.collection("users").authRefresh();
	//	}
	//}, [token]);
	//useInterval(refreshSession, token ? twoMinutesInMs : null);

	return (
		<PocketContext.Provider
			value={{
				//register,
				login,
				logout,
				//user,
				token,
				pb,
				API: api,
			}}
		>
			{children}
		</PocketContext.Provider>
	);


};


function throwExpression(errorMessage: string): never {
	throw new Error(errorMessage);
}

export const usePocket = () => useContext(PocketContext) ?? throwExpression("Missing Pocketbase context!");
