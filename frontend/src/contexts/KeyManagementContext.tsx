// based on https://dev.to/franciscomendes10866/how-to-use-pocketbase-authentication-with-react-context-11be
import type { ReactNode } from 'react'
import {
	createContext,
	useContext,
	useState,
} from "react";
import { User } from '@/lib/api';
import { UserKey } from '@/lib/crypto';
import { KeyImportDialog } from '@/components/KeyImportDialog';
import { toast } from 'react-toastify';
import { KeyGeneratorDialog } from '@/components/KeyGeneratorDialog';
import type { KeyManagementContextType } from '@/types/keymanagement';

const KeyManagementContext = createContext<KeyManagementContextType | null>(null);


export function KeyManagementProvider({ children }: { children: ReactNode }) {
	const [openDialog, setOpenDialog] = useState("NONE" as "NONE" | "IMPORT" | "GENERATE");
	const [justLoggedIn, setJustLoggedIn] = useState(true);
	const [hasKeys, setHasKeys] = useState(false);
	const [user, setUser] = useState(null as User | null);
	const [resultStore,] = useState({ "result": null as UserKey | null, "hasResult": false });


	async function ensureUserHasUnlockedKey(user: User | undefined, justLoggedIn = false): Promise<UserKey | "CANCELED"> {
		if (!user) {
			throw new Error("Es wird für eine Aktion dein E2E-Schlüssel gebraucht, aber du bist noch nicht angemeldet.");
		}
		setUser(user);
		if (user.hasUnlockedKey()) {
			return user.getUnlockedKey()!;
		}
		const hasUnlockableKey = await user.hasUnlockableKey().catch((err: unknown) => {
			console.error(err);
			throw new Error("Fehler bei der E2E-Schlüsselabfrage: " + err?.toString())
		});
		setJustLoggedIn(justLoggedIn);
		setHasKeys(hasUnlockableKey);

		resultStore.hasResult = false;

		if (!hasUnlockableKey) {
			// Nutzer hat noch keinen Schlüssel. Muss neuen generieren.
			setOpenDialog("GENERATE");
		} else {
			// Bestehenden Schlüssel entsperren.
			setOpenDialog("IMPORT");
		}

		// very ugly busy waiting
		while (!resultStore.hasResult) {
			await new Promise((resolve) => { setTimeout(resolve, 10) });
		}

		const result = resultStore.result;
		resultStore.hasResult = false;
		resultStore.result = null;

		if (result !== null) {
			return result;
		} else {
			return "CANCELED";
		}
	}

	async function openGenerateNewKeyDialog(user: User | undefined): Promise<UserKey | "CANCELED"> {
		if (!user) {
			throw new Error("Du musst angemeldet sein um einen neuen Schlüssel zu erstellen.");
		}
		setUser(user);
		setJustLoggedIn(false);
		setHasKeys(true);
		setOpenDialog("GENERATE");

		// very ugly busy waiting
		while (!resultStore.hasResult) {
			await new Promise((resolve) => { setTimeout(resolve, 10) });
		}

		const result = resultStore.result;
		resultStore.hasResult = false;
		resultStore.result = null;

		if (result !== null) {
			return result;
		} else {
			return "CANCELED";
		}
	}

	async function tryUnlockingKey(password: string): Promise<void> {
		if (!user) {
			throw new Error("Du bist nicht angemeldet.");
		}
		const key = await user.unlockKey(password).catch((error) => {
			if (error instanceof AggregateError) {
				throw new Error("Keiner deiner Schlüssel konnte mit dem Passwort entsperrt werden.")
			} else {
				throw error;
			}
		});
		setOpenDialog("NONE");
		resultStore.result = key;
		resultStore.hasResult = true;
		toast.success("Schlüssel erfolgreich entsperrt.")
	}

	async function generateNewKey(password: string): Promise<void> {
		if (!user) {
			throw new Error("Du bist nicht angemeldet.");
		}
		const userkey = await UserKey.generate(user, password);
		if (!userkey.isUnlocked()) {
			throw new Error("Neuer Schlüssel konnte nicht entsperrt werden.");
		}
		await user.addKey(userkey, true);
		setOpenDialog("NONE");
		resultStore.result = userkey;
		resultStore.hasResult = true;
		toast.success("Schlüssel erfolgreich erstellt.")
	}
	return (
		<KeyManagementContext.Provider
			value={{
				ensureUserHasUnlockedKey,
				openGenerateNewKeyDialog,
			}}
		>
			{children}
			<KeyImportDialog
				open={openDialog === "IMPORT"}
				justLoggedIn={justLoggedIn}
				user={user}
				onCancel={function(): void {
					console.log("User pressed cancel on import dialog.")
					setOpenDialog("NONE");
					resultStore.result = null;
					resultStore.hasResult = true;
				}}
				onSubmit={tryUnlockingKey}
				onError={(error) => {
					console.error(error);
					toast.error(error?.toString());
				}}
			/>
			<KeyGeneratorDialog
				open={openDialog === "GENERATE"}
				justLoggedIn={justLoggedIn}
				hasKeys={hasKeys}
				user={user}
				onCancel={function(): void {
					console.log("User pressed cancel on generate dialog.")
					setOpenDialog("NONE");
					resultStore.result = null;
					resultStore.hasResult = true;
				}}
				onCreateRequest={generateNewKey}
				onError={(error) => {
					console.error(error);
					toast.error(error?.toString());
				}}
			/>
		</KeyManagementContext.Provider>
	);


};


function throwExpression(errorMessage: string): never {
	throw new Error(errorMessage);
}

export const useKeyManagement = () => useContext(KeyManagementContext) ?? throwExpression("Missing KeyManagement context!");
