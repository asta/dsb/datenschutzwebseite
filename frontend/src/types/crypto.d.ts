import type { Base64 } from "@/types/base64";

/**
 * The fingerprint (hash of userid+public key) of the verify key (public) of a UserKey.
 */
export type Fingerprint = string;

export type EncryptionAlgorithm = {
	/**
	 * https://developer.mozilla.org/en-US/docs/Web/API/RsaOaepParams
	 * We use RSA-OAEP for asymmetrical encryption because it is the only one available in browsers.
	 */
	name: "RSA-OAEP",
	label_b64?: Base64,
} | {
	/**
	 * https://developer.mozilla.org/en-US/docs/Web/API/AesGcmParams
	 *
	 * We use AES-GCM for symmetrical encryption because it is an "authenticated" encryption.
	 * See https://developer.mozilla.org/en-US/docs/Web/API/SubtleCrypto/encrypt#supported_algorithms
	 *
	 */
	name: "AES-GCM",
	iv_b64: Base64,
	additionalData_b64: Base64,
	tagLength: 128,
}

export interface EncryptedData {
	/**
	 * Encrypted data as Base64 encoded string.
	 *
	 * Note: When using SignedEncryptedData the salt must be included somewhere in the unencrypted data.
	 */
	data_b64: Base64;

	/**
	 * We include this random salt in the data, before signing AND encrypting.
	 * Thus ensuring that no message EVER can result in similar signatures or encryption results!
	 *
	 * Note: During the decryption this salt must also be validated!
	 *
	 * This random salt MUST be included in the data!
	 * It must also be verified after decrypting!
	 *
	 * In case of EncryptedKey the salt will just be used as the salt for deriving the wrap key from the password.
	 */
	salt_b64: Base64;

	/**
	 * Represents algorithm argument that will be used when calling subtle.crypto.encrypt() and subtle.crypto.decrypt().
	 *
	 * Note: This can not be used directly as the argument to those functions.
	 * The Base64 encoded values must be decoded first.
	 *
	 * The Algorithm MUST be validated before giving it to the subtle.crypto functions.
	 * It shouldn't matter, but we never trust user input!
	 */
	algorithm: EncryptionAlgorithm;
}

export interface SignedEncryptedData extends EncryptedData {
	signature: {
		/**
		 * The fingerprint of the UserKey used for signing the data.
		 */
		fingerprint: Fingerprint;
		/**
		 * The signature of the encrypted data.
		 */
		of_encrypted_data_b64: Base64;
		/**
		 * The signature of the unencrypted data.
		 */
		of_unencrypted_data_b64: Base64;
	}
}

export type EncryptedKeyType = "SIGNATURE_KEY" | "ENCRYPTION_KEY";
export interface EncryptedKey extends EncryptedData {
	iterations: number,
	key_type: EncryptedKeyType,
};
