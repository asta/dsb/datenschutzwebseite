import { Room, RoomEvent } from "@/lib/room";
import type { SignedEncryptedData } from "./crypto";
import { User } from "@/lib/api";
import type { Either } from "./utils";

export type SignedEncryptedRoomEvent = SignedEncryptedData;

export type RoomEventID = string;

export enum RoomStatus {
	"SUBMITTING" = "SUBMITTING",
	"OPEN" = "OPEN",
	"CLOSED" = "CLOSED",
}

export enum MessageType {
	"TEXT" = "TEXT",
}

export interface Message {
	type: MessageType.TEXT,
	text: string,
}
export type RoomEventOrError = Either<
	{ error: Error, author?: User, room: Room, unix_timestamp_ms: number, id: string },
	RoomEvent
>;
