import type {
	FormsResponse,
	GroupMembersResponse,
	GroupsResponse,
	UserKeysResponse,
} from "@/types/pocketbase-types";
import type {
	JsonSchema,
	UISchemaElement
} from "@jsonforms/core";

export type FormID = string
export type FormVersion = number
export type UserID = string
export type GroupID = string
export type RoomID = string

export type Form = FormsResponse<UISchemaElement, JsonSchema>;
export interface FormVersions {
	versions: Form[];
	latest: Form;
};

export type UserKey = UserKeysResponse;

export type Group = GroupsResponse & {
	members: GroupMembersResponse[];
};

