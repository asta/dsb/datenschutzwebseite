export type Either<L, R> = {
	left: L,
	right?: undefined,
} | {
	left?: undefined,
	right: R,
}
