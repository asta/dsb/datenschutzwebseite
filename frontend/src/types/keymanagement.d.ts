import type { User } from '@/lib/api';
import type { UserKey } from '@/lib/crypto';

export interface KeyManagementContextType {
	ensureUserHasUnlockedKey: (user: User | undefined, justLoggedIn: boolean) => Promise<UserKey | "CANCELED">;
	openGenerateNewKeyDialog: (user: User | undefined) => Promise<UserKey | "CANCELED">;
};

