import type { API, User } from "@/lib/api";
import type { TypedPocketBase } from "@/types/pocketbase-types";

export interface PocketContextType {
	login: (provider?: string) => Promise<User>;
	logout: () => void;
	token: string;
	pb: TypedPocketBase;
	API: API;
};

