import { StrictMode } from 'react'
import { createRoot } from 'react-dom/client'
import {
	createBrowserRouter,
	RouterProvider,
} from "react-router-dom";
import { PocketProvider, usePocket } from "@/contexts/PocketContext";

import App from '@/App.tsx'
import Forms, { loader as formsLoader } from '@/pages/Forms.tsx'
import Form, { loader as formLoader } from '@/pages/Form.tsx'
import Submissions, { loader as submissionsLoader } from './pages/Submissions';
import Submission, { loader as submissionLoader } from './pages/Submission';
import ErrorBoundary from '@/components/ErrorBoundary'
import ToastContainer from '@/components/ToastConfig';

import './index.css'
import { KeyManagementProvider } from './contexts/KeyManagementContext';


function Routes() {
	const { API } = usePocket()
	const router = createBrowserRouter([
		{
			path: "/",
			element: <App><Forms /></App>,
			loader: () => { return formsLoader({ API }) },
			errorElement: <App><ErrorBoundary /></App>,
		},
		{
			path: "/forms",
			element: <App><Forms /></App>,
			loader: () => { return formsLoader({ API }) },
			errorElement: <App><ErrorBoundary /></App>,
		},
		{
			path: "/form",
			element: <App><Forms /></App>,
			loader: () => { return formsLoader({ API }) },
			errorElement: <App><ErrorBoundary /></App>,
		},
		{
			path: "/form/:formid",
			element: <App isMainContainer={false}><Form /></App>,
			loader: ({ params }) => { return formLoader({ API, params }) },
			errorElement: <App><ErrorBoundary /></App>,
		},
		{
			path: "/form/:formid/:formversion",
			element: <App isMainContainer={false}><Form /></App>,
			loader: ({ params }) => { return formLoader({ API, params }) },
			errorElement: <App><ErrorBoundary /></App>,
		},
		{
			path: "/submissions",
			element: <App reloadPageOnLoginChange><Submissions /></App>,
			loader: () => { return submissionsLoader({ API }) },
			errorElement: <App><ErrorBoundary /></App>,
		},
		{
			path: "/submission",
			element: <App reloadPageOnLoginChange><Submissions /></App>,
			loader: () => { return submissionsLoader({ API }) },
			errorElement: <App><ErrorBoundary /></App>,
		},
		{
			path: "/submission/:submissionid",
			element: <App isMainContainer={true} reloadPageOnLoginChange><Submission /></App>,
			loader: ({ params }) => { return submissionLoader({ API, params }) },
			errorElement: <App><ErrorBoundary /></App>,
		},
	]);
	return <RouterProvider router={router} />
}

createRoot(document.getElementById('root')!).render(
	<StrictMode>
		<PocketProvider>
			<KeyManagementProvider>
				<Routes />
				<ToastContainer />
			</KeyManagementProvider>
		</PocketProvider>
	</StrictMode>,
)
