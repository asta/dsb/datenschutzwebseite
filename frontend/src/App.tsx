import type { ReactNode } from 'react'
import { useMemo } from 'react'

import { toast } from 'react-toastify';

import { usePocket } from "@/contexts/PocketContext";

import { Button } from "@/components/ui/button"

import Link from "@/components/LinkWrapper"
import { DropdownMenu, DropdownMenuTrigger, DropdownMenuContent, DropdownMenuItem, DropdownMenuSeparator, DropdownMenuLabel } from "@/components/ui/dropdown-menu"
import { Avatar, AvatarImage, AvatarFallback } from "@/components/ui/avatar"

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHouse, faScroll, faEnvelopeOpenText } from '@fortawesome/free-solid-svg-icons'
import { useKeyManagement } from './contexts/KeyManagementContext';
import { useNavigate } from 'react-router-dom';

function App({
	children,
	isMainContainer = true,
	reloadPageOnLoginChange = false,
}: {
	children: ReactNode,
	isMainContainer?: boolean,
	reloadPageOnLoginChange?: boolean,
}) {
	const { ensureUserHasUnlockedKey, openGenerateNewKeyDialog } = useKeyManagement();
	const { API, login, logout } = usePocket();
	const navigate = useNavigate();
	const user = API.currentuser;
	const twoletterusername = useMemo(() => {
		return user?.username.replace(/[^0-9a-z]/gi, '').toUpperCase().substring(0, 2) ?? "NA"
	}, [user])


	return (
		<div className="flex flex-col min-h-dvh">
			<header className="bg-primary text-primary-foreground px-4 md:px-6 py-3 flex items-center justify-between">
				<Link href="#" className="flex items-center gap-2" prefetch={false}>
					<FontAwesomeIcon icon={faHouse} />
					<span className="font-semibold text-lg">Formulartool</span>
				</Link>
				<nav className="flex items-center gap-4 mx-auto">
					<Link
						href="/forms"
						className="text-sm font-medium hover:underline underline-offset-4 flex items-center gap-2"
						prefetch={false}
					>
						<FontAwesomeIcon icon={faScroll} />
						Formulare
					</Link>
					<Link
						href="/submissions"
						className="text-sm font-medium hover:underline underline-offset-4 flex items-center gap-2"
						prefetch={false}
					>
						<FontAwesomeIcon icon={faEnvelopeOpenText} />
						Einreichungen
					</Link>
				</nav>
				<nav className="flex items-center gap-4">
					{!user ?
						<Button className="flex items-center gap-2 font-medium bg-green-500 text-white rounded-md px-3 py-1" onClick={() => {
							login().then(async (user) => {
								toast.success(<span>Anmeldung erfolgreich. Hallo {API.currentuser?.username}.</span>);
								await ensureUserHasUnlockedKey(user, true);
								if (reloadPageOnLoginChange) {
									//navigate(0); // buggy. It does a full page reload.
									navigate("#", {
										replace: true,
									});
								}
							}).catch((err: unknown) => {
								console.error(err);
								toast.error(err?.toString())
							})
						}}>Login</Button>
						:
						<>
							<DropdownMenu>
								<DropdownMenuTrigger asChild>
									<Avatar className="h-9 w-9 bg-muted">
										<AvatarImage src="/placeholder-user.jpg" alt="@shadcn" />
										<AvatarFallback className="text-muted-foreground">{twoletterusername}</AvatarFallback>
									</Avatar>
								</DropdownMenuTrigger>
								<DropdownMenuContent>
									<DropdownMenuLabel>@{user.username}</DropdownMenuLabel>
									<DropdownMenuSeparator />
									<DropdownMenuItem>My Account</DropdownMenuItem>
									<DropdownMenuItem onClick={() => {
										openGenerateNewKeyDialog(user).catch((err: unknown) => {
											console.error(err);
											toast.error(err?.toString())
										})
									}}>Erstelle Schlüssel</DropdownMenuItem>
									<DropdownMenuItem>Settings</DropdownMenuItem>
									<DropdownMenuSeparator />
									<DropdownMenuItem onClick={() => {
										logout();
										if (reloadPageOnLoginChange) {
											//navigate(0); // buggy. It does a full page reload.
											navigate("#", {
												replace: true,
											});
										}
									}}>Logout</DropdownMenuItem>
								</DropdownMenuContent>
							</DropdownMenu>
						</>
					}
				</nav>
			</header>
			<main className={"flex-1" + (isMainContainer ? " container xl:py-16 py-4" : "")}>
				{children}
			</main>
			<footer className="bg-muted p-6 md:py-12 w-full">
				<div className="container max-w-7xl grid grid-cols-2 sm:grid-cols-3 md:grid-cols-5 gap-8 text-sm">
					<div className="grid gap-1">
						<h3 className="font-semibold">Company</h3>
						<Link href="#" prefetch={false}>
							About Us
						</Link>
						<Link href="#" prefetch={false}>
							Our Team
						</Link>
						<Link href="#" prefetch={false}>
							Careers
						</Link>
						<Link href="#" prefetch={false}>
							News
						</Link>
					</div>
					<div className="grid gap-1">
						<h3 className="font-semibold">Forms</h3>
						<Link href="/forms" prefetch={false}>
							All Forms
						</Link>
						<Link href="/submissions" prefetch={false}>
							Submissions
						</Link>
						<Link href="#" prefetch={false}>
							Templates
						</Link>
						<Link href="#" prefetch={false}>
							Analytics
						</Link>
					</div>
					<div className="grid gap-1">
						<h3 className="font-semibold">Resources</h3>
						<Link href="#" prefetch={false}>
							Documentation
						</Link>
						<Link href="#" prefetch={false}>
							Support
						</Link>
						<Link href="#" prefetch={false}>
							FAQs
						</Link>
						<Link href="#" prefetch={false}>
							Tutorials
						</Link>
					</div>
					<div className="grid gap-1">
						<h3 className="font-semibold">Legal</h3>
						<Link href="#" prefetch={false}>
							Privacy Policy
						</Link>
						<Link href="#" prefetch={false}>
							Terms of Service
						</Link>
						<Link href="#" prefetch={false}>
							Cookie Policy
						</Link>
					</div>
					<div className="grid gap-1">
						<h3 className="font-semibold">Contact</h3>
						<Link href="#" prefetch={false}>
							Sales
						</Link>
						<Link href="#" prefetch={false}>
							Support
						</Link>
						<Link href="#" prefetch={false}>
							Partnerships
						</Link>
						<Link href="#" prefetch={false}>
							Press
						</Link>
					</div>
				</div>
			</footer>
		</div>
	)
}

export default App
