import { Separator } from '@/components/ui/separator';
import { Badge } from "@/components/ui/badge"
import { Room } from '@/lib/room';
import { Form } from '@/types/api';
import { useEffect, useState } from 'react';
import { User } from '@/lib/api';
import { Avatar, AvatarImage, AvatarFallback } from "@/components/ui/avatar"
import { getFormRecipients } from './FormSubmitButton';
import { usePocket } from '@/contexts/PocketContext';

function InfoSidebar({
	form,
	room,
	onError,
}: {
	form: Form,
	room?: Room,
	onError: (error: unknown) => void,
}) {
	const { API } = usePocket();
	const [participants, setParticipants] = useState<User[] | null>(null);
	useEffect(() => {
		if (room) {
			room.getRecipients().then((record) => {
				const users = Object.keys(record).map((userid) => record[userid]);
				users.sort((a, b) => a.username.localeCompare(b.username));
				setParticipants(users);
			}).catch(onError);
		} else {
			getFormRecipients(API, form, API.currentuser ? [API.currentuser] : []).then((res) => {
				const users = Object.keys(res.recipients).map((userid) => res.recipients[userid]);
				users.sort((a, b) => a.username.localeCompare(b.username));
				setParticipants(users);
			}).catch(onError);
		}
	}, [API, form, onError, room]);
	return (<>
		<div className="hidden">
			<h2 className="text-xl font-bold">Informationen</h2>
		</div>
		<div className="space-y-2 border-b border-muted pb-4">
			<div>
				<span className="text-sm font-medium block">Formular</span>
				<span className="text-muted-foreground text-sm block">{form.title}</span>
			</div>
			<Separator />
			<div>
				<span className="text-sm font-medium block">Beschreibung</span>
				<p className="text-muted-foreground text-sm">{form.description}</p>
			</div>
			<Separator />
			<div>
				<span className="text-sm font-medium block">Kennung</span>
				<span className="text-muted-foreground text-sm block">{form.formid} (v{form.formversion})</span>
			</div>
			<Separator />
			<div>
				<span className="text-sm font-medium block">Veröffentlicht</span>
				<span className="text-muted-foreground text-sm block">{form.created}</span>
			</div>
			<Separator />
			<div>
				<span className="text-sm font-medium block">Empfänger</span>
				<div className="flex items-center gap-2">
					TODO
					{/*
					<Popover>
						<PopoverTrigger asChild>
							<Avatar>
								<AvatarImage src="/placeholder-user.jpg" />
								<AvatarFallback>JD</AvatarFallback>
							</Avatar>
						</PopoverTrigger>
						<PopoverContent className="p-4 space-y-2">
							<div className="font-semibold">John Doe</div>
							<p className="text-muted-foreground">Finance Team</p>
						</PopoverContent>
					</Popover>
					<Popover>
						<PopoverTrigger asChild>
							<Avatar>
								<AvatarImage src="/placeholder-user.jpg" />
								<AvatarFallback>JA</AvatarFallback>
							</Avatar>
						</PopoverTrigger>
						<PopoverContent className="p-4 space-y-2">
							<div className="font-semibold">Jane Appleseed</div>
							<p className="text-muted-foreground">Accounting</p>
						</PopoverContent>
					</Popover>
					<Popover>
						<PopoverTrigger asChild>
							<Avatar>
								<AvatarImage src="/placeholder-user.jpg" />
								<AvatarFallback>SM</AvatarFallback>
							</Avatar>
						</PopoverTrigger>
						<PopoverContent className="p-4 space-y-2">
							<div className="font-semibold">Sarah Mayer</div>
							<p className="text-muted-foreground">Finance Manager</p>
						</PopoverContent>
					</Popover>
				*/}
				</div>
			</div>
			<div className="grid gap-1">
				<div className="text-sm font-medium">Participants</div>
				<Separator className="my-2" />
				<div className="grid gap-2">
					{participants ? participants.map((participant, index) => (
						<div key={index} className="flex items-center justify-between gap-2">
							<div className="flex items-center gap-2">
								<Avatar>
									<AvatarImage src="/placeholder-user.jpg" alt={participant.name} />
									<AvatarFallback>{participant.name.charAt(0)}</AvatarFallback>
								</Avatar>
								<div>
									<div className="font-medium">{participant.name}</div>
									<div className="text-xs text-muted-foreground">{participant.username}</div>
								</div>
							</div>
						</div>
					)) : <p>Lade Nutzer...</p>}
				</div>
			</div>
			<Separator className="my-4" />
			<div className="grid gap-1">
				<div className="text-sm font-medium">Chat Info</div>
				<Separator className="my-2" />
				<div className="grid gap-2 text-sm">
					<div className="flex items-center justify-between">
						<span>Created</span>
						<span>2023-04-01</span>
					</div>
					<Separator className="my-2" />
					<div className="flex items-center justify-between">
						<span>Status</span>
						<span className="font-medium text-green-500">Active</span>
					</div>
					<Separator className="my-2" />
					<div className="flex items-center justify-between">
						<span>Labels</span>
						<div className="flex gap-2">
							<Badge variant="outline" className="px-2 py-1 text-xs">
								Project
							</Badge>
							<Badge variant="outline" className="px-2 py-1 text-xs">
								Team
							</Badge>
						</div>
					</div>
				</div>
			</div>
		</div>

	</>)
}


export default InfoSidebar
