import type { ReactNode } from "react"
import * as RRD from "react-router-dom";

function Link({ children, href, className = "" }: { children: ReactNode, href: string, prefetch?: boolean, className?: string }) {
	if (className) {
		return (
			<RRD.Link to={href} className={className}>{children}</RRD.Link>
		)
	} else {
		return (
			<RRD.Link to={href}>{children}</RRD.Link>
		)

	}
}

export default Link
