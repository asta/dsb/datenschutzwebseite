import type { JsonSchema, UISchemaElement } from '@jsonforms/core';
import type { FormsResponse } from '@/types/pocketbase-types'

import { createTypstCompiler } from '@myriaddreamin/typst.ts/dist/esm/compiler.mjs';
import { createTypstRenderer } from '@myriaddreamin/typst.ts/dist/esm/renderer.mjs';
import type { TypstCompiler } from '@myriaddreamin/typst.ts/dist/esm/compiler.mjs';
import type { TypstRenderer } from '@myriaddreamin/typst.ts/dist/esm/renderer.mjs';
import { useCallback, useEffect, useState } from 'react';

import typst_ts_renderer_bg_wasm from '@myriaddreamin/typst-ts-renderer/pkg/typst_ts_renderer_bg.wasm?url'
import typst_ts_web_compiler_bg_wasm from '@myriaddreamin/typst-ts-web-compiler/pkg/typst_ts_web_compiler_bg.wasm?url'

import {
	Alert,
	AlertDescription,
	AlertTitle,
} from "@/components/ui/alert"
import { AlertCircle } from 'lucide-react';
import { preloadFontAssets } from '@myriaddreamin/typst.ts/dist/esm/options.init.mjs';
import { Label } from "@/components/ui/label"
import { Switch } from "@/components/ui/switch"

export type File = {
	path: string,
	content: Uint8Array,
	type: "bytes",
} | {
	path: string,
	content: string,
	type: "string",
} | {
	path: string,
	content: object,
	type: "object",
};

export type CompileResult = Awaited<ReturnType<TypstCompiler["compile"]>>

const MAINFILEPATH = '/main.typ';

const encoder = new TextEncoder();

async function createCompiler() {
	const cc /* compiler */ = createTypstCompiler();
	await cc.init({
		beforeBuild: [
			preloadFontAssets({
				assets: ["text", "cjk", "emoji"], // preload all assets
				assetUrlPrefix: "/assets/fonts", // use self hosted fonts
				// fetcher: already using browser caching,
			}),
			// maybe load further custom fonts?
		],
		getModule: () => typst_ts_web_compiler_bg_wasm,
	});
	return cc;
}

async function createRenderer() {
	const renderer = createTypstRenderer();
	await renderer.init({
		getModule: () => typst_ts_renderer_bg_wasm,
	});
	return renderer;
}

function loadFileIntoContext(compiler: TypstCompiler, file: File) {
	// TODO test if it is even needed to unmap the files
	compiler.unmapShadow(file.path);

	let content = null;
	switch (file.type) {
		case "bytes":
			content = file.content;
			break;
		case "string":
			content = encoder.encode(file.content);
			break;
		case "object":
			content = encoder.encode(JSON.stringify(file.content, undefined, undefined));
			break;
		default:
			throw new Error("Unknown file content type");
	}
	compiler.mapShadow(file.path, content);
}

async function compile(
	compiler: TypstCompiler,
	data: object,
	highlightData: boolean,
): Promise<CompileResult> {
	loadFileIntoContext(compiler, {
		path: "/data.json",
		content: data,
		type: "object",

	});

	const settings = {
		highlightData,
	};
	loadFileIntoContext(compiler, {
		path: "/settings.json",
		content: settings,
		type: "object",

	});

	const res = await compiler.compile({
		mainFilePath: MAINFILEPATH,
		format: "vector",
		diagnostics: "full",
	});
	return res
}

function FormOutput({
	form,
	data,
	defaultHighlightData,
	extraFiles,
	resizable,
	collapsible,
	onCompile,
	onError,
}: {
	form: FormsResponse<UISchemaElement, JsonSchema>,
	data: object,
	defaultHighlightData: boolean,
	extraFiles: File[],
	resizable: boolean,
	collapsible: boolean,
	onCompile: (compileResult: CompileResult) => void,
	onError?: (error: unknown) => void,
}) {
	const [error, setError] = useState(undefined as unknown);
	const onErrorWrapper = useCallback((error: unknown) => {
		setError(error);
		if (onError !== undefined) {
			onError(error);
		}
	}, [onError]);

	const [compiler, setCompiler] = useState(undefined as TypstCompiler | undefined);
	const [compileResult, setCompileResult] = useState({} as CompileResult);
	const [renderer, setRenderer] = useState(undefined as TypstRenderer | undefined);
	const [unsafeSVG, setUnsafeSVG] = useState(undefined as string | undefined);
	const [loading, setLoading] = useState(true);
	const [highlightData, setHighlightData] = useState(defaultHighlightData);
	const [collapsed, setCollapsed] = useState(false);

	// load and initialize compiler
	useEffect(() => {
		if (compiler || error) {
			// This useEffect has no dependency and thus runs on every render.
			// We return early if compiler was already created. Also prevent a nasty infinite loop.
			return;
		}
		console.info("Creating typst compiler...")
		createCompiler().then((cc) => {
			cc.resetShadow(); // ensure no other files exist in the context

			// add extra files as context (shadow) to compiler
			extraFiles.forEach(file => {
				loadFileIntoContext(cc, file);
			});

			// add main file (template)
			loadFileIntoContext(cc, {
				path: MAINFILEPATH,
				content: form.template,
				//content: "Hallo Welt",
				//content: data.name + "yeet",
				//content: "#set page(width: 10em, height: 20em, margin: 0em)\n\n#set text(red); Hello, World",
				//content: "#yeetset page(width: 10em, height: 20em, margin: 0em)\n\n#set text(red); Hello, World",
				type: "string",
			});

			// add information about the form itself
			loadFileIntoContext(cc, {
				path: "/forminfo.json",
				content: form,
				type: "object",
			});

			setCompiler(cc);
			console.info("Typst compiler created.")
		}).catch(onErrorWrapper);
	});

	// load and initialize renderer
	useEffect(() => {
		if (renderer || error) {
			// This useEffect has no dependency and thus runs on every render.
			// We return early if renderer was already created. Also prevent a nasty infinite loop.
			return;
		}
		console.info("Creating typst renderer...")
		createRenderer().then((renderer) => {
			setRenderer(renderer);
			console.info("Typst renderer created.")
		}).catch(onErrorWrapper);
	});

	// (re-)compile
	useEffect(() => {
		if (!compiler) { return; }
		compile(compiler, data, highlightData).then((res) => {
			setCompileResult(res);
			onCompile(res);
		}).catch(onErrorWrapper);
	}, [compiler, data, highlightData, onErrorWrapper, onCompile]);

	// (re-)render
	useEffect(() => {
		if (!compiler) { return; }
		if (!renderer) { return; }
		if (!compileResult.result) { return; }
		renderer.renderSvg({
			format: "vector",
			artifactContent: compileResult.result,
			// window: TODO maybe use window?,
			// renderSession: TODO maybe use renderSession?,
			data_selection: {
				body: true,
				defs: true,
				css: true,
				js: false, // TODO why is js needed in the SVG?
			},
		}).then(setUnsafeSVG).catch(onErrorWrapper);
		// using renderSVG instead of  renderToSvg because renderToSvg has a Bug throwing errors: Error: recursive use of an object detected which would lead to unsafe aliasing in rust
		// renderToCanvas is ugly and text is not selectable
	}, [compiler, renderer, compileResult, onErrorWrapper]);

	const [svgDataURL, setSVGDataURL] = useState(undefined as string | undefined);
	useEffect(() => {
		if (!unsafeSVG) { return; }

		// We don't trust unsafeSVG.
		// Instead we load it through an img element as a data url.
		// By loading it via an img element we will have it completely sandboxed, as browsers don't trust images.
		// Thus even if somehow a custom script tag or similar is loaded into the SVG it will not be executed.
		// TODO Add source to these claims! (I know LiveOverflow talked about this in a video, but I can't find a source right now.)
		//
		// Vorteil: Rechtsklick -> Save Image As... funktioniert.
		// Nachteil: Dadurch, dass das Element jetzt nur noch ein Bild ist, ist der Text nicht mehr auswählbar/kopierbar.
		// TODO unsafeSVG in DOMPurify werfen und via dangerouslySetInnerHTML direkt in Seite einbetten.

		const unsafeSVGBytes = encoder.encode(unsafeSVG);
		// https://developer.mozilla.org/en-US/docs/Web/API/File/File
		//const unsafeSVGFile = new File(unsafeSVGBytes, "unsafeSVG.svg", { type: "image/svg+xml" });
		const unsafeSVGBlob = new Blob([unsafeSVGBytes], {
			type: "image/svg+xml",
		});

		const url = URL.createObjectURL(unsafeSVGBlob);
		//const url = "data:image/svg+xml;base64," + window.btoa(unsafeSVG); // cannot handle unicode characters in unsafeSVG
		//console.log(url);

		setSVGDataURL((prevUrl) => {
			if (prevUrl) {
				// free old url from previous render to prevent memory leaking
				URL.revokeObjectURL(prevUrl);
				// TODO there is still a smaller memory leak, because the url is not revoked when the element is destroyed
			}
			return url;
		});
		setLoading(false);
	}, [unsafeSVG]);

	return (<>
		<div className="flex items-center justify-between">
			{collapsible ?
				<div className="flex items-center gap-2">
					<Label htmlFor="collapsed-changes" className="text-sm">
						Einklappen
					</Label>
					<Switch checked={collapsed} onCheckedChange={(checked) => { setCollapsed(checked); }} id="collapsed-changes" aria-label="Collapsed changes" />
				</div>
				: null}
			{/* TODO select format between SVG, PDF, and JSON (data and full)*/}
			<div className="flex items-center gap-2">
				<Label htmlFor="highlight-changes" className="text-sm">
					Werte hervorheben
				</Label>
				<Switch checked={highlightData} onCheckedChange={(checked) => { setHighlightData(checked); }} id="highlight-changes" aria-label="Highlight changes" />
			</div>
		</div>
		<div className={resizable ? "overflow-auto resize h-[80rem] w-[56.532rem]" : "w-full"}>
			{compileResult.diagnostics?.length || error
				? <Alert variant="destructive">
					<AlertCircle className="h-4 w-4" />
					<AlertTitle >Fehler</AlertTitle>
					<AlertDescription>
						{compileResult.diagnostics?.length ? <>
							<span>Beim Bauen des Dokumentes sind Fehler aufgetreten:</span>
							<ul className="list-disc list-inside">
								{compileResult.diagnostics.map((error) => {
									if (typeof error === "string") {
										return (<li key={error}>{error}</li>)
									} else {

										return (<li key={JSON.stringify(error)}><pre>{JSON.stringify(error, null, 4)}</pre></li>)
									}
								})}
							</ul></> : null}
						{error ? <>
							<span>Es ist ein generischer Fehler aufgetreten:</span>
							<p>{(error as unknown)?.toString()}</p>
						</> : null}
					</AlertDescription>
				</Alert >
				: null}
			{loading
				? <p>Lade...</p>
				: <img className="bg-white w-full" src={svgDataURL} alt="Formular" />
			}
		</div>

	</>)
}

export default FormOutput
