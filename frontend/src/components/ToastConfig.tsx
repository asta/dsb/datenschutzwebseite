import * as toastify from "react-toastify";
//import "react-toastify/dist/ReactToastify.css";
import "react-toastify/dist/ReactToastify.min.css";

// https://fkhadra.github.io/react-toastify/introduction/

// use via importing the toast function and calling it.
// import { toast } from 'react-toastify';
// toast(<>Hello World</>)

// TestContainer must be used somewhere in the App.

function ToastContainer() {
	return (<toastify.ToastContainer
		position="bottom-right"
		autoClose={5000}
		hideProgressBar={false}
		newestOnTop={false}
		closeOnClick={true}
		rtl={false}
		pauseOnFocusLoss={true}
		draggable={true}
		pauseOnHover={true}
		theme="light"
		transition={toastify.Bounce}
	/>)
}


export default ToastContainer;
