import { useRouteError } from 'react-router-dom'
import { AlertCircle } from "lucide-react"

import {
	Alert,
	AlertDescription,
	AlertTitle,
} from "@/components/ui/alert"

function ErrorBoundary() {
	const error = useRouteError();
	console.error(error);

	return (
		<Alert variant="destructive">
			<AlertCircle className="h-4 w-4" />
			<AlertTitle>Fehler</AlertTitle>
			<AlertDescription>
				<span>Ein Fehler ist aufgetreten.</span>
				<p>{error?.toString()}</p>
				<pre>
					{JSON.stringify(error, null, 4)}
				</pre>
				<span>Type: {typeof error}</span>
			</AlertDescription>
		</Alert>
	)
}

export default ErrorBoundary
