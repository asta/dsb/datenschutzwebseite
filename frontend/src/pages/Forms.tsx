//import './Forms.css'

import { Card, CardHeader, CardTitle, CardDescription, CardFooter } from "@/components/ui/card"
import Link from "@/components/LinkWrapper"
import { useLoaderData } from 'react-router-dom'
import type { API } from "@/lib/api"
import { FormID, FormVersions } from "@/types/api"
import {
	DropdownMenu,
	DropdownMenuContent,
	DropdownMenuItem,
	DropdownMenuLabel,
	DropdownMenuSeparator,
	DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCaretDown } from '@fortawesome/free-solid-svg-icons'
import { Button } from "@/components/ui/button"

export async function loader({ API }: { API: API }) {
	const forms = await API.getMultipleFormVersions()
	return forms
}

function FormCards({ forms }: { forms: Record<FormID, FormVersions> }) {
	const formids = Object.keys(forms).toSorted();
	if (formids.length > 0) {
		return formids.map(formid => {
			return <FormCard key={formid} formVersions={forms[formid]} />
		});
	} else {
		return <p>Aktuell sind keine Formulare vorhanden.</p>
	}
}

function FormCard({ formVersions }: { formVersions: FormVersions }) {

	const latest = formVersions.latest;

	const formVersionMenuItems = formVersions.versions.toSorted((a, b) => b.formversion - a.formversion).map((form) => {
		return <DropdownMenuItem key={"v" + form.formversion.toString()}>
			<Link
				href={"/form/" + form.formid + "/" + form.formversion}
			>
				v{form.formversion} <span className="text-muted-foreground text-sm">({form.created})</span>
			</Link>
		</DropdownMenuItem>
	});

	return (<Card className="bg-white dark:bg-gray-800">
		<CardHeader className="pb-0">
			<CardTitle className="text-primary-600">{latest.title}</CardTitle>
			<CardDescription className="text-gray-600">{latest.description}</CardDescription>
		</CardHeader>
		<CardFooter className="flex justify-between items-center pt-0">
			<div className="flex items-center space-x-2">
				<div id="feedback-version" className="text-gray-600">
					<DropdownMenu>
						<DropdownMenuTrigger asChild>
							<Button variant="outline" className="h-8">
								Version
								<FontAwesomeIcon icon={faCaretDown} className="px-2" />
							</Button>
						</DropdownMenuTrigger>
						<DropdownMenuContent align="start">
							<DropdownMenuLabel>Formularversion</DropdownMenuLabel>
							<DropdownMenuSeparator />
							{formVersionMenuItems}
						</DropdownMenuContent>
					</DropdownMenu>
				</div>
			</div>
			<Link
				href={"/form/" + latest.formid}
				className="inline-flex h-8 items-center rounded-md border border-primary-600 bg-white px-4 text-sm font-medium text-primary-600 shadow-sm transition-colors hover:bg-primary-100 hover:text-primary-700 focus-visible:outline-none focus-visible:ring-1 focus-visible:ring-primary-700 disabled:pointer-events-none disabled:opacity-50 dark:border-gray-800 dark:bg-gray-950 dark:hover:bg-gray-950 dark:hover:text-gray-50 dark:focus-visible:ring-gray-300"
			>
				Ausfüllen
			</Link>
		</CardFooter>
	</Card>)
}

function Forms() {
	const forms = useLoaderData() as Awaited<ReturnType<API['getMultipleFormVersions']>>

	return (
		<div className="">
			<h2 className="text-5xl font-normal leading-normal mt-0 mb-2 text-primary-800 px-12">
				Formulare
			</h2>
			<div className="grid gap-4 px-4 sm:grid-cols-2 lg:gap-6 xl:grid-cols-3">
				<FormCards forms={forms} />
			</div>
		</div>
	)
}

export default Forms
