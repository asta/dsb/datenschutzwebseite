import { API } from "@/lib/api";
import { useLoaderData, useNavigate } from "react-router-dom";

import {
	ColumnDef,
	ColumnFiltersState,
	SortingState,
	VisibilityState,
	flexRender,
	getCoreRowModel,
	getFilteredRowModel,
	getPaginationRowModel,
	getSortedRowModel,
	useReactTable,
} from "@tanstack/react-table"
import { ArrowUpDown, ChevronDown } from "lucide-react"

import { Button } from "@/components/ui/button"
import {
	DropdownMenu,
	DropdownMenuCheckboxItem,
	DropdownMenuContent,
	DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu"
import {
	Table,
	TableBody,
	TableCell,
	TableHead,
	TableHeader,
	TableRow,
} from "@/components/ui/table"
import { useState } from "react";
import { Room } from "@/lib/room";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowRight } from "@fortawesome/free-solid-svg-icons";
import { formatRelativeTime } from "@/lib/utils";



export async function loader({ API }: { API: API }) {
	let rooms: Room[] = [];
	if (API.currentuser) {
		const roomsrecord = await API.getRooms([], false);
		rooms = Object.keys(roomsrecord).map((roomid) => {
			return roomsrecord[roomid];
		});
		rooms.sort((a, b) => Date.parse(a.created) - Date.parse(b.created));
	}
	return {
		rooms,
		API,
	};
}


function Submissions() {
	const { rooms, API } = useLoaderData() as { rooms: Room[], API: API }
	const data = rooms;

	// reload page when user logs in, to reload data
	//useEffect(() => {
	//	console.debug("began effect");
	//	if (API.currentuser) {
	//		console.debug("user is logged in");
	//		loader({ API }).then(({ rooms }) => {
	//			console.debug("user is logged in");
	//			setData(rooms);
	//		}).catch((err: unknown) => {
	//			console.error(err);
	//			toast.error(err?.toString());
	//		});
	//	}
	//	console.debug("finished effect");
	//}, [API, API.currentuser]);


	const [sorting, setSorting] = useState<SortingState>([])
	const [columnFilters, setColumnFilters] = useState<ColumnFiltersState>(
		[]
	)
	const [columnVisibility, setColumnVisibility] =
		useState<VisibilityState>({})
	const [rowSelection, setRowSelection] = useState({})
	const navigate = useNavigate();

	const columns: ColumnDef<Room>[] = [
		{
			id: "subject",
			header: ({ column }) => {
				return (
					<Button
						variant="ghost"
						onClick={() => column.toggleSorting(column.getIsSorted() === "asc")}
					>
						Betreff
						<ArrowUpDown className="ml-2 h-4 w-4" />
					</Button>
				)
			},
			cell: ({ row }) => {
				return <span>{row.original.subject}</span>
			},
			enableSorting: true,
			enableHiding: false,
		},
		{
			id: "form",
			header: ({ column }) => {
				return (
					<Button
						variant="ghost"
						onClick={() => column.toggleSorting(column.getIsSorted() === "asc")}
					>
						Formular
						<ArrowUpDown className="ml-2 h-4 w-4" />
					</Button>
				)
			},
			cell: ({ row }) => {
				return <span>{row.original.form.formid} (v{row.original.form.formversion})</span>
			},
			enableSorting: true,
			enableHiding: true,
		},
		{
			id: "creator",
			header: ({ column }) => {
				return (
					<Button
						variant="ghost"
						onClick={() => column.toggleSorting(column.getIsSorted() === "asc")}
					>
						Antragsteller
						<ArrowUpDown className="ml-2 h-4 w-4" />
					</Button>
				)
			},
			cell: ({ row }) => {
				return <span>@{row.original.creator.username}</span>
			},
			enableSorting: true,
			enableHiding: true,
		},
		{
			id: "created",
			header: ({ column }) => {
				return (
					<Button
						variant="ghost"
						onClick={() => column.toggleSorting(column.getIsSorted() === "asc")}
					>
						Datum
						<ArrowUpDown className="ml-2 h-4 w-4" />
					</Button>
				)
			},
			cell: ({ row }) => {
				const date = new Date(row.original.created);
				return <time dateTime={date.toISOString()} title={date.toLocaleString("de")}>{formatRelativeTime(date)}</time>
			},
			enableSorting: true,
			enableHiding: true,
		},
		/*
		{
			id: "select",
			header: ({ table }) => (
				<Checkbox
					checked={
						table.getIsAllPageRowsSelected() ||
						(table.getIsSomePageRowsSelected() && "indeterminate")
					}
					onCheckedChange={(value) => table.toggleAllPageRowsSelected(!!value)}
					aria-label="Select all"
				/>
			),
			cell: ({ row }) => (
				<Checkbox
					checked={row.getIsSelected()}
					onCheckedChange={(value) => row.toggleSelected(!!value)}
					aria-label="Select row"
				/>
			),
			enableSorting: false,
			enableHiding: false,
		},
		*/
		/*
		 {
			 accessorKey: "status",
			 header: "Status",
			 cell: ({ row }) => (
				 <div className="capitalize">{row.getValue("status")}</div>
			 ),
		 },
		 */
		{
			id: "visit",
			header: "Öffnen",
			cell: ({ row }) => {
				return (
					<Button
						variant="outline"
						type="button"
						onClick={() => {
							navigate("/submission/" + row.original.id);
						}}
					>
						Öffnen
						<FontAwesomeIcon icon={faArrowRight} className="ml-2 h-4 w-4" />
					</Button>
				)
			},
			enableSorting: false,
			enableHiding: true,
		},
	]

	const table = useReactTable({
		data,
		columns,
		onSortingChange: setSorting,
		onColumnFiltersChange: setColumnFilters,
		getCoreRowModel: getCoreRowModel(),
		getPaginationRowModel: getPaginationRowModel(),
		getSortedRowModel: getSortedRowModel(),
		getFilteredRowModel: getFilteredRowModel(),
		onColumnVisibilityChange: setColumnVisibility,
		onRowSelectionChange: setRowSelection,
		state: {
			sorting,
			columnFilters,
			columnVisibility,
			rowSelection,
		},
	})


	return (
		<div className="w-full">
			<div className="flex items-center py-4">
				{/*
				<Input
					placeholder="Filter emails..."
					value={(table.getColumn("email")?.getFilterValue() as string) ?? ""}
					onChange={(event) =>
						table.getColumn("email")?.setFilterValue(event.target.value)
					}
					className="max-w-sm"
				/>
				*/}
				<DropdownMenu>
					<DropdownMenuTrigger asChild>
						<Button variant="outline" className="ml-auto">
							Spalten <ChevronDown className="ml-2 h-4 w-4" />
						</Button>
					</DropdownMenuTrigger>
					<DropdownMenuContent align="end">
						{table
							.getAllColumns()
							.filter((column) => column.getCanHide())
							.map((column) => {
								return (
									<DropdownMenuCheckboxItem
										key={column.id}
										className="capitalize"
										checked={column.getIsVisible()}
										onCheckedChange={(value) =>
											column.toggleVisibility(!!value)
										}
									>
										{column.id}
									</DropdownMenuCheckboxItem>
								)
							})}
					</DropdownMenuContent>
				</DropdownMenu>
			</div>
			<div className="rounded-md border">
				<Table>
					<TableHeader>
						{table.getHeaderGroups().map((headerGroup) => (
							<TableRow key={headerGroup.id}>
								{headerGroup.headers.map((header) => {
									return (
										<TableHead key={header.id}>
											{header.isPlaceholder
												? null
												: flexRender(
													header.column.columnDef.header,
													header.getContext()
												)}
										</TableHead>
									)
								})}
							</TableRow>
						))}
					</TableHeader>
					<TableBody>
						{table.getRowModel().rows?.length ? (
							table.getRowModel().rows.map((row) => (
								<TableRow
									key={row.id}
									data-state={row.getIsSelected() && "selected"}
									onClick={() => {
										navigate("/submission/" + row.original.id);
									}}
									onKeyUp={(e) => {
										if (e.key === "Enter") {
											e.currentTarget.click();
										}
									}}
									role="link"
									className="cursor-pointer pointer-events-auto"
									tabIndex={0}
								>
									{row.getVisibleCells().map((cell) => (
										<TableCell key={cell.id}>
											{flexRender(
												cell.column.columnDef.cell,
												cell.getContext()
											)}
										</TableCell>
									))}
								</TableRow>
							))
						) : (
							<TableRow>
								<TableCell
									colSpan={columns.length}
									className="h-24 text-center"
								>
									{API.currentuser ? <>Es wurden noch keine Einreichungen mit dir geteilt.</> : <>Du musst dich anmelden um deine Einreichungen zu sehen.</>}
								</TableCell>
							</TableRow>
						)}
					</TableBody>
				</Table>
			</div>
		</div>
	)
}

export default Submissions
