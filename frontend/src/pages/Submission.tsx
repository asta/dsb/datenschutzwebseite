import { Params, useLoaderData } from 'react-router-dom'

import { HTMLAttributes, useEffect, useState } from "react"
import { Button } from "@/components/ui/button"
import { Avatar, AvatarImage, AvatarFallback } from "@/components/ui/avatar"
import { Textarea } from "@/components/ui/textarea"

import { API } from '@/lib/api';
import { Room, RoomEventMessage, RoomEventStatusChange, RoomEventSubmissionValue } from '@/lib/room';
import type { Message, RoomEventOrError } from '@/types/room'
import { MessageType } from '@/types/room'
import { RoomEventsTypeOptions } from '@/types/pocketbase-types'
import { toast } from 'react-toastify'
import { formatRelativeTime } from '@/lib/utils'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faComment, faFileLines, faGear, faImage, faScroll, faTriangleExclamation, faUserPlus } from '@fortawesome/free-solid-svg-icons'
import { Tabs, TabsList, TabsTrigger, TabsContent } from "@/components/ui/tabs"
import { JsonForms } from '@jsonforms/react'
import { Form } from '@/types/api'
import { materialCells, materialRenderers } from '@jsonforms/material-renderers'
import FormOutput from '@/components/FormOutput'
import InfoSidebar from '@/components/InfoSidebar'

export async function loader({ API, params }: { API: API, params: Params<string> }) {
	const submissionid = params.submissionid;
	if (!submissionid) {
		throw new Error("No submissionid given.")
	}
	const room = await API.getRoom(submissionid).catch(() => {
		// eslint-disable-next-line @typescript-eslint/only-throw-error
		throw new Response("Einreichung nicht gefunden.", { status: 404 });
	});
	return room;
}

function SubmissionValue({ form, event }: { form: Form, event: RoomEventSubmissionValue }) {
	return <div className="flex-1 flex flex-col md:flex-row w-full">
		<div className="flex-1 md:border-r border-muted w-full">
			<Tabs defaultValue="formOutput" className="w-full">
				<TabsList className="border-b grid grid-cols-2 gap-2 md:border-b-0 md:grid-cols-2">
					<TabsTrigger value="form" className="flex-1" title="Werte">
						<FontAwesomeIcon icon={faFileLines} className="p-1" />
						Werte
					</TabsTrigger>
					<TabsTrigger value="formOutput" className="flex-1" title="Formular">
						<FontAwesomeIcon icon={faImage} className="p-1" />
						Formular
					</TabsTrigger>
				</TabsList>
				<TabsContent value="form" className="p-6 md:p-8 space-y-6">
					<JsonForms
						schema={form.schema ?? undefined}
						uischema={form.layout ?? undefined}
						data={event.submissionvalue}
						renderers={materialRenderers}
						cells={materialCells}
						readonly
					/>
				</TabsContent>
				<TabsContent value="formOutput" className="p-3 md:p-4 space-y-6">
					<FormOutput
						form={form}
						data={event.submissionvalue}
						defaultHighlightData={false}
						extraFiles={[]}
						resizable={true}
						collapsible={false}
						onCompile={() => {
							// TODO
						}}
						onError={(error) => {
							console.error(error);
							//toast.error(JSON.stringify(error));
							toast.error((error as Error).toString());
						}}
					/>
				</TabsContent>
			</Tabs>
		</div>
	</div>
}

function Event({ form, eventorerror }: { form: Form, eventorerror: RoomEventOrError }) {
	if (eventorerror.left) {
		const event = eventorerror.left;
		const date = new Date(event.unix_timestamp_ms);
		return (<li className="mb-6 ms-6">
			<span className="absolute flex items-center justify-center w-4 h-4 rounded-full -start-2 ring-8 ring-white">
				<FontAwesomeIcon className="bg-white w-4 h-4 text-red-500" icon={faTriangleExclamation} />
			</span>
			<div className={"grid gap-2 rounded-lg bg-muted/40  p-4 border border-gray-200 shadow-sm"}>
				<div className={"items-center justify-between text-xs text-muted-foreground flex"}>
					<div className="flex items-center gap-2">
						<div className="flex items-center gap-2">
							{event.author ? <Avatar>
								<AvatarImage src="/placeholder-user.jpg" alt={event.author.name} />
								<AvatarFallback>{event.author.name.charAt(0)}</AvatarFallback>
							</Avatar> : null}
							<span className="font-medium">
								Das Event von @{event.author?.username} erstellt <time dateTime={date.toISOString()} title={date.toLocaleString("de")}>{formatRelativeTime(date)}</time> konnte nicht angezeigt werden.
							</span>
						</div>
					</div>
				</div>
				<div className="flex items-start gap-4">
					<div className="text-sm">
						{event.error.toString()}
					</div>
				</div>
			</div>
		</li>)
	}
	const event = eventorerror.right;
	const date = new Date(event.unix_timestamp_ms);
	const inline = (event.type === RoomEventsTypeOptions.MEMBER_ADD || event.type === RoomEventsTypeOptions.STATUS_CHANGE);
	return (<li className="mb-6 ms-6">
		<span className="absolute flex items-center justify-center w-4 h-4 rounded-full -start-2 ring-8 ring-white">
			{event.type === RoomEventsTypeOptions.MESSAGE /*        */ && <FontAwesomeIcon className="bg-white w-4 h-4 text-primary" icon={faComment} />}
			{event.type === RoomEventsTypeOptions.MEMBER_ADD /*     */ && <FontAwesomeIcon className="bg-white w-4 h-4 text-green-500" icon={faUserPlus} />}
			{event.type === RoomEventsTypeOptions.STATUS_CHANGE /*  */ && <FontAwesomeIcon className="bg-white w-4 h-4 text-blue-500" icon={faGear} />}
			{event.type === RoomEventsTypeOptions.SUBMISSIONVALUE /**/ && <FontAwesomeIcon className="bg-white w-4 h-4 text-orange-500" icon={faScroll} />}
		</span>
		<div className={"grid gap-2 rounded-lg bg-muted/40 " + (!inline ? " p-4 border border-gray-200 shadow-sm" : "")}>
			<div className={"items-center justify-between text-xs text-muted-foreground" + (!inline ? "flex" : "inline-flex")}>
				<div className="flex items-center gap-2">
					<div className="flex items-center gap-2">
						{!inline ? <Avatar>
							<AvatarImage src="/placeholder-user.jpg" alt={event.author.name} />
							<AvatarFallback>{event.author.name.charAt(0)}</AvatarFallback>
						</Avatar> : null}
						<span className="font-medium">
							@{event.author.username} hat <time dateTime={date.toISOString()} title={date.toLocaleString("de")}>{formatRelativeTime(date)}</time>
							{event.type === RoomEventsTypeOptions.MESSAGE /*        */ && <span> geschrieben:</span>}
							{event.type === RoomEventsTypeOptions.MEMBER_ADD /*     */ && <span> eine Person hinzugefügt: TODO</span>}
							{event.type === RoomEventsTypeOptions.STATUS_CHANGE /*  */ && <span> den Status auf {(event as RoomEventStatusChange).status} gesetzt.</span>}
							{event.type === RoomEventsTypeOptions.SUBMISSIONVALUE /**/ && <span> ein Formular eingereicht:</span>}
						</span>
					</div>
				</div>
			</div>
			{!inline ?
				<div className="flex items-start gap-4">
					{event.type === RoomEventsTypeOptions.MESSAGE && <div className="text-sm">
						{(event as RoomEventMessage).message.text}
					</div>}
					{event.type === RoomEventsTypeOptions.SUBMISSIONVALUE &&
						<SubmissionValue form={form} event={event as RoomEventSubmissionValue} />
					}
				</div>
				: null}
		</div>
	</li>)

}

function Submission() {
	const room = useLoaderData() as Room;
	const [events, setEvents] = useState<RoomEventOrError[]>([]);
	useEffect(() => {
		const timer = setInterval(() => {
			room.getEvents().then(setEvents).catch((err) => {
				console.error(err);
			});
		}, 5000);
		return () => clearInterval(timer);
	});

	const [newMessage, setNewMessage] = useState("")
	const [isSidebarCollapsed, setIsSidebarCollapsed] = useState(false)

	return (
		<div className="grid min-h-screen w-full lg:grid-cols-[1fr_320px]">
			<div className={`flex flex-col ${isSidebarCollapsed ? "lg:hidden" : ""}`}>
				<div className="sticky top-0 z-10 flex h-14 items-center border-b bg-muted/40 px-4">
					<Button variant="ghost" size="icon" className="mr-4 lg:hidden" onClick={() => {
						setIsSidebarCollapsed(!isSidebarCollapsed)
					}}>
						<MenuIcon className="h-5 w-5" />
						<span className="sr-only">Toggle Sidebar</span>
					</Button>
					<h2 className="text-lg font-semibold">{room.subject}</h2>
				</div>
				<div className="flex-1 overflow-auto p-4">
					{events.length > 0 ?

						<ol className="relative border-s border-gray-200">
							{events.map((eventorerror) => (
								<Event key={eventorerror.right ? eventorerror.right.getId() : eventorerror.left.id} form={room.form} eventorerror={eventorerror} />
							))}
						</ol>
						: <p>Lade...</p>}
				</div>
				<div className="max-w-2xl w-full sticky bottom-0 mx-auto py-2 flex flex-col gap-1.5 px-4 bg-background">
					<form className="relative" onSubmit={(event) => {
						event.preventDefault();
						const message: Message = {
							type: MessageType.TEXT,
							text: newMessage,
						};
						room.addEventMessage(message).then(() => {
							setNewMessage("");
							// update UI?
						}).catch((err: unknown) => {
							console.error(err);
							toast.error(err?.toString());
						});
					}}>
						<Textarea
							placeholder="Schreibe eine Nachricht..."
							name="message"
							id="message"
							rows={1}
							value={newMessage}
							onChange={(e) => {
								setNewMessage(e.target.value)
							}}
							className="min-h-[48px] rounded-2xl resize-none p-4 border border-neutral-400 shadow-sm pr-16"
						/>
						<Button type="submit" size="icon" className="absolute w-8 h-8 top-3 right-3">
							<ArrowUpIcon className="w-4 h-4" />
							<span className="sr-only">Senden</span>
						</Button>
					</form>
				</div>
			</div>
			<div className={`flex flex-col border-l bg-muted/40 p-4 ${isSidebarCollapsed ? "hidden lg:flex" : ""}`}>
				<InfoSidebar form={room.form} room={room} onError={(error) => {
					console.error(error);
					//toast.error(JSON.stringify(error));
					toast.error((error as Error).toString());
				}} />
			</div>
		</div >
	)
}

function ArrowUpIcon(props: HTMLAttributes<SVGSVGElement>) {
	return (
		<svg
			{...props}
			xmlns="http://www.w3.org/2000/svg"
			width="24"
			height="24"
			viewBox="0 0 24 24"
			fill="none"
			stroke="currentColor"
			strokeWidth="2"
			strokeLinecap="round"
			strokeLinejoin="round"
		>
			<path d="m5 12 7-7 7 7" />
			<path d="M12 19V5" />
		</svg>
	)
}


function MenuIcon(props: HTMLAttributes<SVGSVGElement>) {
	return (
		<svg
			{...props}
			xmlns="http://www.w3.org/2000/svg"
			width="24"
			height="24"
			viewBox="0 0 24 24"
			fill="none"
			stroke="currentColor"
			strokeWidth="2"
			strokeLinecap="round"
			strokeLinejoin="round"
		>
			<line x1="4" x2="20" y1="12" y2="12" />
			<line x1="4" x2="20" y1="6" y2="6" />
			<line x1="4" x2="20" y1="18" y2="18" />
		</svg>
	)
}


export default Submission
