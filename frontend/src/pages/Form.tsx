import { Params, useLoaderData } from 'react-router-dom'

import type { ErrorObject } from 'ajv';
import {
	materialRenderers,
	materialCells,
} from '@jsonforms/material-renderers';
import { useMemo, useState } from 'react';
import type { ReactNode } from 'react'
import { JsonForms } from '@jsonforms/react';
import * as A from '@/types/api'

import { AlertCircle } from "lucide-react"
import {
	Alert,
	AlertDescription,
	AlertTitle,
} from "@/components/ui/alert"
import { toast } from 'react-toastify';

import {
	ResizableHandle,
	ResizablePanel,
	ResizablePanelGroup,
} from "@/components/ui/resizable"
import { Tabs, TabsList, TabsTrigger, TabsContent } from "@/components/ui/tabs"
//import { Popover, PopoverTrigger, PopoverContent } from "@/components/ui/popover"
//import { Avatar, AvatarImage, AvatarFallback } from "@/components/ui/avatar"
import { faEye, faPenToSquare, faCircleInfo } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useMediaQuery } from 'react-responsive'
import FormOutput from '@/components/FormOutput';
import { API } from '@/lib/api';
import { FormSubmitButton } from '@/components/FormSubmitButton';
import InfoSidebar from '@/components/InfoSidebar';

export async function loader({ API, params }: { API: API, params: Params<string> }) {
	const formid = params.formid;
	if (!formid) {
		throw new Error("No formid given.")
	}
	const formversion = parseInt(params.formversion ?? "-1");
	if (isNaN(formversion)) {
		throw new Response("Formversion muss eine valide Zahl sein.", { status: 400 }); // eslint-disable-line @typescript-eslint/only-throw-error
	}
	const formVersions = await API.getFormVersions(formid);
	let form;
	if (formversion === -1) {
		form = formVersions.latest;
	} else {
		form = formVersions.versions.find((f) => f.formversion === formversion)
		if (!form) {
			throw new Response("Formular existert, aber nicht in der Version " + formversion.toString() + ". Aktuellste Version: " + formVersions.latest.formversion.toString(), { status: 404 }); // eslint-disable-line @typescript-eslint/only-throw-error
		}
	}
	return form
}

function FormViewDesktop({ formTab, formOutputTab, detailsTab }: { formTab: ReactNode, formOutputTab: ReactNode, detailsTab: ReactNode }) {
	return (
		<ResizablePanelGroup direction="horizontal" className="flex-1 md:border-r border-muted">
			<ResizablePanel defaultSize={50} minSize={30}>
				<div className="p-6 md:p-8 space-y-6">
					{formTab}
				</div>
			</ResizablePanel>
			<ResizableHandle withHandle />
			<ResizablePanel defaultSize={38} minSize={20}>
				<div className="p-3 md:p-4 space-y-6">
					{formOutputTab}
				</div>
			</ResizablePanel>
			<ResizableHandle />
			<ResizablePanel defaultSize={12} minSize={7}>
				<div className="p-3 md:p-4 space-y-6">
					{detailsTab}
				</div>
			</ResizablePanel>
		</ResizablePanelGroup>
	)
}

function FormViewMobile({ formTab, formOutputTab, detailsTab }: { formTab: ReactNode, formOutputTab: ReactNode, detailsTab: ReactNode }) {
	return (
		<div className="flex-1 flex flex-col md:flex-row">
			<div className="flex-1 md:border-r border-muted">
				<Tabs defaultValue="form" className="h-full">
					<TabsList className="border-b grid grid-cols-3 gap-2 md:border-b-0 md:grid-cols-3">
						<TabsTrigger value="form" className="flex-1" title="Formular">
							<FontAwesomeIcon icon={faPenToSquare} className="p-1" />
							Formular
						</TabsTrigger>
						<TabsTrigger value="formOutput" className="flex-1" title="Vorschau">
							<FontAwesomeIcon icon={faEye} className="p-1" />
							Ausgabe
						</TabsTrigger>
						<TabsTrigger value="details" className="flex-1" title="Informationen">
							<FontAwesomeIcon icon={faCircleInfo} className="p-1" />
							Informationen
						</TabsTrigger>
					</TabsList>
					<TabsContent value="form" className="p-6 md:p-8 space-y-6">
						{formTab}
					</TabsContent>
					<TabsContent value="formOutput" className="p-3 md:p-4 space-y-6">
						{formOutputTab}
					</TabsContent>
					<TabsContent value="details" className="p-6 md:p-8 space-y-6">
						{detailsTab}
					</TabsContent>
				</Tabs>
			</div>
		</div>
	)
}

function Form({ viewModeBreakpointRem = (1280 / 16) }: { viewModeBreakpointRem?: number }) {
	const form = useLoaderData() as A.Form;
	const schema = form.schema ?? undefined;
	const uischema = form.layout ?? undefined;
	const initialData = {};

	const [data, setData] = useState(initialData);
	const [errors, setErrors] = useState([] as ErrorObject[] | undefined);

	const formTab = useMemo(() => <>
		<div className="space-y-4">
			<JsonForms
				schema={schema}
				uischema={uischema}
				data={data}
				renderers={materialRenderers}
				cells={materialCells}
				onChange={(state) => {
					setData(state.data as object)
					setErrors(state.errors as ErrorObject[] | undefined)
				}}
			/>
			<FormSubmitButton
				disabled={(errors?.length ?? 1) > 0}
				form={form}
				data={data}
			>
				Formular Verbindlich Einreichen
			</FormSubmitButton>
			{
				errors?.length ?
					<Alert variant="destructive">
						<AlertCircle className="h-4 w-4" />
						<AlertTitle hidden>Formularfehler</AlertTitle>
						<AlertDescription>
							<span>Behebe die folgenden Fehler vor dem Einreichen:</span>
							<ul className="list-disc list-inside">
								{errors.map((error) =>
									<li key={JSON.stringify(error)}>
										<code>
											{error.message}
											{((typeof error.data === 'string') || (typeof error.data === 'object' && "length" in error.data && (error.data as { length: number })?.length)) ? <>: {JSON.stringify(error.data, null, 4)}</> : null}
											{/*JSON.stringify(error, null, 4)*/}
										</code>
									</li>
								)}
							</ul>
						</AlertDescription>
					</Alert >
					:
					<p>
						Keine Fehler. Das Formular kann eingereicht werden.
					</p>
			}
		</div>
	</>, [form, data, errors, schema, uischema]);

	const formOutputTab = useMemo(() => {
		return (<>
			<h2 className="text-xl font-bold">Ausgabe</h2>
			<FormOutput
				form={form}
				data={data}
				defaultHighlightData={true}
				extraFiles={[]}
				resizable={false}
				collapsible={false}
				onCompile={() => {
					// TODO
				}}
				onError={(error) => {
					console.error(error);
					//toast.error(JSON.stringify(error));
					toast.error((error as Error).toString());
				}}
			/>
		</>)
	}, [form, data]);

	const detailsTab = useMemo(() => <>
		<InfoSidebar form={form} onError={(error) => {
			console.error(error);
			//toast.error(JSON.stringify(error));
			toast.error((error as Error).toString());
		}} />
	</>, [form]);

	const isMobile = useMediaQuery({ query: "(max-width: " + viewModeBreakpointRem.toFixed(0) + "rem)" });
	if (isMobile) {
		return <FormViewMobile formTab={formTab} formOutputTab={formOutputTab} detailsTab={detailsTab} />
	} else {
		return <FormViewDesktop formTab={formTab} formOutputTab={formOutputTab} detailsTab={detailsTab} />
	}
}

export default Form
