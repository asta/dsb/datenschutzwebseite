import type {
	Form,
	FormID,
	FormVersions,
	Group,
	GroupID,
	UserID,
	RoomID,
	//RoomID,
	//UserKey
} from "@/types/api";
import type {
	Collections,
	GroupMembersResponse,
	GroupsResponse,
	RoomEventsRecord,
	RoomEventsResponse,
	RoomEventsTypeOptions,
	RoomKeysRecord,
	RoomKeysResponse,
	RoomsRecord,
	RoomsResponse,
	RoomsTypeOptions,
	//GroupMembersPermissionsOptions,
	//GroupMembersResponse,
	//RoomKeysResponse,
	//RoomsTypeOptions,
	//UserKeysRecord
	TypedPocketBase,
	UserKeysResponse,
	UsersResponse,
} from "@/types/pocketbase-types";
import type { EncryptedKey, Fingerprint, SignedEncryptedData } from "@/types/crypto";
import type { RecordFullListOptions } from "pocketbase";
import { UserKey } from "./crypto";
import { RoomEventID, SignedEncryptedRoomEvent } from "@/types/room";
import { Room } from "./room";

export class User implements UsersResponse {
	avatar: string;
	name: string;
	email: string;
	emailVisibility: boolean;
	username: string;
	verified: boolean;
	id: string;
	created: string;
	updated: string;
	collectionId: string;
	collectionName: Collections;
	expand?: unknown;
	API: API;

	protected userkeyscache: Record<Fingerprint, UserKey>;
	protected unlockedkey?: UserKey;

	/**
	 * Don't use this constructor directly.
	 * Use API.getUser() or API.getUsers() instead.
	 *
	 *
	 * The only ones allowed to call this constructor is API.getUsers() and PocketContext.tsx!
	 */
	constructor(API: API, user: UsersResponse) {
		this.API = API;
		this.avatar = user.avatar;
		this.name = user.name;
		this.email = user.email;
		this.emailVisibility = user.emailVisibility;
		this.username = user.username;
		this.verified = user.verified;
		this.id = user.id;
		this.created = user.created;
		this.updated = user.updated;
		this.collectionId = user.collectionId;
		this.collectionName = user.collectionName;
		this.expand = user.expand;
		this.userkeyscache = {};
	}

	async getUserKeys(include_private_keys: boolean): Promise<Record<Fingerprint, UserKey>> {
		const fingerprints = Object.keys(this.userkeyscache);
		if (fingerprints.length <= 0 || (include_private_keys && !(fingerprints.every((fingerprint) => this.userkeyscache[fingerprint].isUnlockable())))) {
			const rawkeys = await this.API.getUserKeys(this.id, include_private_keys);
			const userkeys = await Promise.all(rawkeys.map(rawkey => {
				return UserKey.import(this, rawkey);
			}));
			this.userkeyscache = {};

			userkeys.forEach(userkey => {
				this.userkeyscache[userkey.fingerprint] = userkey;
			});
		}

		return this.userkeyscache;
	}

	hasUnlockedKey(): boolean {
		return this.unlockedkey?.isUnlocked() ?? false;
	}

	async hasUnlockableKey(): Promise<boolean> {
		if (this.hasUnlockedKey()) {
			return true;
		}
		const keys = await this.getUserKeys(true);
		const fingerprints = Object.keys(keys);
		for (const fingerprint of fingerprints) {
			if (keys[fingerprint].isUnlockable()) {
				return true;
			} else {
				// this.getUserKeys() already ensures that every key is unlockable, we don't need to iterate over every key.
				return false;
			}
		}
		return false;
	}

	async unlockKey(password: string): Promise<UserKey> {
		if (this.hasUnlockedKey()) {
			return this.unlockedkey!;
		}
		const keys = await this.getUserKeys(true);
		const fingerprints = Object.keys(keys);
		this.unlockedkey = await Promise.any(fingerprints.map((fingerprint) => keys[fingerprint].unlock(password)));
		return this.unlockedkey;
	}

	async addKey(key: UserKey, setasunlockedkey = true): Promise<void> {
		if (key.user.id !== this.id) {
			throw new Error("Cannot add key that doesn't belong to me.")
		}
		await this.API.uploadUserKey(key);
		this.userkeyscache[key.fingerprint] = key;
		if (setasunlockedkey) {
			this.unlockedkey = key;
		}
	}

	getUnlockedKey(): UserKey | undefined {
		return this.unlockedkey;
	}

	lockAllKeys(): void {
		const keys = this.userkeyscache;
		const fingerprints = Object.keys(keys);
		fingerprints.forEach(fingerprint => {
			keys[fingerprint].lock();
		});
		this.unlockedkey?.lock()
		this.unlockedkey = undefined;
	}

}

export class API {
	pb: TypedPocketBase;
	/**
	 * currentuser is stateful. (You can use it as a dependency in for example useEffect.)
	 */
	currentuser?: User;

	protected usercache: Record<UserID, User>;
	protected roomcache: Record<RoomID, Room>;

	constructor(pb: TypedPocketBase) {
		this.pb = pb;
		this.currentuser = undefined;
		this.usercache = {};
		this.roomcache = {};
	};

	/**
	 * This function is only allowed to be called from PocketContext.tsx. As user has to be stateful.
	 */
	setCurrentUser(user: User | undefined) {
		// this function is implicity called by PocketContext.tsx every time the authStore changes, because setUser is called.

		if (this.currentuser && user !== this.currentuser && this.currentuser.id in this.usercache) {
			// delete user from usercache on logout
			this.currentuser.lockAllKeys()
			delete this.usercache[this.currentuser.id];
		}
		this.currentuser = user;
		if (user) {
			this.usercache[user.id] = user;
		}
	}

	async getMultipleFormVersions(formids?: FormID[]): Promise<Record<FormID, FormVersions>> {
		const options: RecordFullListOptions = {
			sort: 'formversion',
		};

		if (formids && formids.length > 0) {
			options.filter = formids.map((formid) => this.pb.filter('formid = {:formid}', { formid })).join(" || ");
		}
		const forms: Form[] = await this.pb.collection("forms").getFullList(options);
		const res: Record<FormID, FormVersions> = {};
		forms.forEach((form) => {
			const formid = form.formid;
			if (!(formid in res)) {
				res[formid] = {
					versions: [],
					latest: form,
				}
			}
			res[formid].versions.push(form);
			if (form.formversion > res[formid].latest.formversion) {
				res[formid].latest = form;
			}
		});
		return res;
	}

	async getFormVersions(formid: FormID): Promise<FormVersions> {
		const forms = await this.getMultipleFormVersions([formid]);
		if (!(formid in forms)) {
			throw new Error("API response does not contain expected form " + formid);
		}
		return forms[formid];
	}

	async getSpecificForm(id: string): Promise<Form> {
		return await this.pb.collection("forms").getOne(id);
	}

	async getUsers(userids: UserID[], ignoreCache = false): Promise<Record<UserID, User>> {
		if (userids.length <= 0) {
			return {};
		}
		const res: Record<UserID, User> = {};
		if (!ignoreCache) {
			userids.filter((userid) => (userid in this.usercache)).map((userid) => {
				res[userid] = this.usercache[userid];
			});

			// update userids to keep only those that are not cached
			userids = userids.filter((userid) => !(userid in this.usercache));
		}
		if (userids.length > 0) {
			const options: RecordFullListOptions = {
				filter: userids.map((userid) => this.pb.filter('id = {:userid}', { userid })).join(" || "),
			};
			const rawusers: UsersResponse[] = await this.pb.collection("users").getFullList(options);
			rawusers.forEach((rawuser) => {
				const user = new User(this, rawuser);
				res[user.id] = user;
				this.usercache[user.id] = user; // update cache
			});
		}
		return res;
	}

	async getUser(userid: UserID, ignoreCache = false): Promise<User> {
		const users = await this.getUsers([userid], ignoreCache);
		if (!(userid in users)) {
			throw new Error("API response does not contain expected user " + userid);
		}
		return users[userid];
	}

	async getUserKeys(userid: UserID, include_private_keys: boolean): Promise<UserKeysResponse<EncryptedKey, EncryptedKey>[]> {
		const options: RecordFullListOptions = {
			filter: this.pb.filter('user = {:userid}', { userid }),
		};

		const collection = include_private_keys ? "user_keys" : "user_keys_publiconly";

		return await this.pb.collection(collection).getFullList(options);
	}

	async uploadUserKey(userkey: UserKey): Promise<void> {
		return await this.pb.collection("user_keys").create({
			id: userkey.getId(),
			...userkey.toUserKeyRecord(),
		});
	}

	async getGroups(groupids?: GroupID[]): Promise<Group[]> {
		// TODO optimze this function, only do one request to the database
		const options: RecordFullListOptions = {};
		if (groupids && groupids.length > 0) {
			options.filter = groupids.map((groupid) => this.pb.filter('id = {:groupid}', { groupid })).join(" || ");
		}
		const rawgroups: GroupsResponse[] = await this.pb.collection("groups").getFullList(options);
		const rawgroupids = rawgroups.map((rawgroup) => rawgroup.id);
		const rawmembers: GroupMembersResponse[] = await this.pb.collection("group_members").getFullList({
			filter: rawgroupids.map((groupid) => this.pb.filter('id = {:groupid}', { groupid })).join(" || "),
		});
		return rawgroups.map((rawgroup) => {
			const group: Group = {
				...rawgroup,
				// TODO optmize this, the filter always goes through all members of all groups, which is dumb
				members: rawmembers.filter((rawmember) => rawmember.group === rawgroup.id),
			};
			return group;
		});
	}

	async getGroup(groupid: GroupID): Promise<Group> {
		const groups = await this.getGroups([groupid]);
		if (groups.length != 1) {
			throw new Error("Expected only single record in API response.");
		}
		return groups[0];
	}

	/*
	async addUserToGroup(group: Group, user: User, permissions?: GroupMembersPermissionsOptions[], reason?: object): Promise<GroupMembersResponse> {
		throw new Error("TODO");
	}

	async removeUserFromGroup(group: Group, user: User): Promise<void> {
		throw new Error("TODO");
	}

	async setUserPermissionsInGroup(group: Group, user: User, permissions: GroupMembersPermissionsOptions[]): Promise<void> {
		throw new Error("TODO");
	}
	*/

	async getRooms(roomids?: RoomID[], ignoreCache = false): Promise<Record<RoomID, Room>> {
		let res: Record<RoomID, Room> = {};
		const includeAllRooms = (!roomids || roomids.length == 0);
		console.debug("includeAllRooms", includeAllRooms);
		console.debug("roomcache", this.roomcache);
		const options: RecordFullListOptions = {};
		if (!roomids) {
			roomids = [];
		}
		let apirequestnecessary = true;
		if (!ignoreCache) {
			if (includeAllRooms) {
				res = { ...this.roomcache };
			} else {
				roomids.filter((roomid) => (roomid in this.roomcache)).forEach((roomid) => {
					res[roomid] = this.roomcache[roomid];
				});
			}
		}

		if (includeAllRooms && !ignoreCache) {
			const blacklistroomids = [...Object.keys(this.roomcache)];
			options.filter = blacklistroomids.map((roomid) => this.pb.filter('id != {:roomid}', { roomid })).join(" && ");
		} else if (!includeAllRooms && !ignoreCache) {
			const whitelistroomids = roomids.filter((roomid) => !(roomid in this.roomcache));
			options.filter = whitelistroomids.map((roomid) => this.pb.filter('id = {:roomid}', { roomid })).join(" || ");
			apirequestnecessary = whitelistroomids.length > 0;
		} else if (!includeAllRooms) {
			const whitelistroomids = roomids;
			options.filter = whitelistroomids.map((roomid) => this.pb.filter('id = {:roomid}', { roomid })).join(" || ");
			apirequestnecessary = roomids.length > 0;
		}
		if (apirequestnecessary) {
			const rawrooms: RoomsResponse[] = await this.pb.collection("rooms").getFullList(options);
			await Promise.all(rawrooms.map(async (rawroom) => {
				const room: Room = await Room.build(this, rawroom);
				res[room.id] = room;
				this.roomcache[room.id] = room; // update cache
			}));
		}
		return res;
	}

	async getRoom(roomid: RoomID): Promise<Room> {
		const rooms = await this.getRooms([roomid]);
		if (!(roomid in rooms)) {
			throw new Error("API response does not contain expected room " + roomid);
		}
		return rooms[roomid];
	}

	async createRoom(type: RoomsTypeOptions, subject: string, form: Form, member_users: User[], member_groups: Group[]): Promise<Room> {
		if (!this.currentuser) {
			throw new Error("Du musst angemeldet sein um einen Raum zu erstellen.");
		}
		const data: RoomsRecord = {
			"type": type,
			"subject": subject,
			"creator": this.currentuser.id,
			"form": form.id,
			"member_users": member_users.map((user) => user.id),
			"member_groups": member_groups.map((group) => group.id),
		};

		const roomrecord = await this.pb.collection('rooms').create(data);
		const room: Room = await Room.build(this, roomrecord);
		this.roomcache[room.id] = room;
		return room;
	}

	async uploadRoomKey(room: Room, target: UserKey, roomkey_encrypted: SignedEncryptedData): Promise<RoomKeysResponse> {
		if (!this.currentuser) {
			throw new Error("Du musst angemeldet sein um einen Schlüssel zu einem Raum hinzuzufügen.");
		}
		const data: RoomKeysRecord = {
			"room": room.id,
			"target": target.getId(),
			"source": this.currentuser.id,
			"roomkey_encrypted": roomkey_encrypted
		};

		return await this.pb.collection('room_keys').create(data);
	}

	async getEncryptedRoomKeys(room: Room): Promise<RoomKeysResponse<SignedEncryptedData>[]> {
		return await this.pb.collection('room_keys').getFullList({
			filter: this.pb.filter('room = {:roomid}', { roomid: room.id }),
		});
	}

	async uploadRoomEvent(room: Room, type: RoomEventsTypeOptions, event_encrypted: SignedEncryptedRoomEvent, eventid: RoomEventID): Promise<RoomEventsResponse> {
		if (!this.currentuser) {
			throw new Error("Du musst angemeldet sein um eine Nachricht zu senden.");
		}
		const data: RoomEventsRecord = {
			"author": this.currentuser.id,
			"type": type,
			"room": room.id,
			"event_encrypted": event_encrypted,
		};

		const res = await this.pb.collection('room_events').create({
			id: eventid,
			...data,
		});
		return res;
	}

	async getRoomEvents(room: Room): Promise<RoomEventsResponse<SignedEncryptedRoomEvent>[]> {
		const options: RecordFullListOptions = {
			filter: this.pb.filter('room = {:roomid}', { roomid: room.id }),
		};
		return await this.pb.collection("room_events").getFullList(options);
	}
}

