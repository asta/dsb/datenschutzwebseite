import type { UserKeysPubliconlyResponse, UserKeysRecord, UserKeysResponse } from "@/types/pocketbase-types";
import type { Base64 } from "@/types/base64";
import type { EncryptedData, EncryptedKey, EncryptedKeyType, EncryptionAlgorithm, Fingerprint, SignedEncryptedData } from "@/types/crypto";
import { User } from "@/lib/api";
import { base64_decode_bytes, base64_decode_json, base64_encode_bytes, base64_encode_json } from "./base64";
import { Room } from "./room";

const signature_algoritm = {
	// https://developer.mozilla.org/en-US/docs/Web/API/SubtleCrypto/sign#supported_algorithms
	/**
	 * https://developer.mozilla.org/en-US/docs/Web/API/EcdsaParams
	 */
	name: "ECDSA",
	/**
	 * We use SHA-256 instead of SHA-384 (which is arguably more secure than SHA-512), because we use P-256 for the signature keys.
	 *
	 * See also https://soatok.blog/2022/05/19/guidance-for-choosing-an-elliptic-curve-signature-algorithm-in-2022/
	 */
	hash: "SHA-256",
}

async function calculate_fingerprint(user: User, unencrypted_key_b64: Base64): Promise<Fingerprint> {
	/**
	 * The key is actually base64 encoded json, but we care about the raw bytes.
	 *
	 * The first step in base64_decode_json is also to call base64_decode_bytes, so this is okay to do.
	 */
	const key_bytes = base64_decode_bytes(unencrypted_key_b64);
	const key_string = new TextDecoder().decode(key_bytes);
	const bytes = new TextEncoder().encode(user.id + "." + key_string);
	const hash_buffer = await crypto.subtle.digest("SHA-384", bytes);

	// convert digest to hex string.
	// based on https://developer.mozilla.org/en-US/docs/Web/API/SubtleCrypto/digest#converting_a_digest_to_a_hex_string
	const hash_array = Array.from(new Uint8Array(hash_buffer));
	const hash_hex = hash_array.map((b) => b.toString(16).padStart(2, "0")).join("");
	return hash_hex;
}

async function derive_wrapkey_from_password(password: string, salt_b64: Base64, iterations: number): Promise<CryptoKey> {
	// https://developer.mozilla.org/en-US/docs/Web/API/SubtleCrypto/deriveKey#pbkdf2_derive_aes_key_from_password
	// derive PBKDF2 key from the password that can be used to derive the actual key
	const derive_key = await crypto.subtle.importKey(
		"raw",
		new TextEncoder().encode(password),
		"PBKDF2",
		false,
		["deriveBits", "deriveKey"],
	);

	// derive the actual key.
	const wrap_key = await crypto.subtle.deriveKey(
		{
			name: "PBKDF2",
			salt: base64_decode_bytes(salt_b64),
			iterations: iterations,
			hash: "SHA-512",
		},
		derive_key,
		{ name: "AES-GCM", length: 256 },
		true,
		["wrapKey", "unwrapKey"],
	);
	return wrap_key;
}

async function encrypt_key_with_password(key: CryptoKey, user: User, key_type: EncryptedKeyType, password: string): Promise<EncryptedKey> {
	if (password.length < 16) {
		throw new Error("Refusing to encrypt key with key that is way too short!");
	}

	const iterations = 500000;
	const salt_b64 = base64_encode_bytes(crypto.getRandomValues(new Uint8Array(32)));
	const wrap_key = await derive_wrapkey_from_password(password, salt_b64, iterations);

	const iv = crypto.getRandomValues(new Uint8Array(32)); // https://nvlpubs.nist.gov/nistpubs/Legacy/SP/nistspecialpublication800-38d.pdf#%5B%7B%22num%22%3A65%2C%22gen%22%3A0%7D%2C%7B%22name%22%3A%22XYZ%22%7D%2C0%2C792%2Cnull%5D
	const additionalData = new TextEncoder().encode("A private key owned by " + user.id + " used as " + key_type);

	// https://developer.mozilla.org/en-US/docs/Web/API/AesGcmParams
	const algorithm: EncryptionAlgorithm = {
		name: "AES-GCM",
		iv_b64: base64_encode_bytes(iv),
		additionalData_b64: base64_encode_bytes(additionalData),
		tagLength: 128,
	}
	const encrypted_key_bytes = await crypto.subtle.wrapKey("jwk", key, wrap_key, {
		name: algorithm.name,
		iv: iv,
		additionalData: additionalData,
		tagLength: algorithm.tagLength,
	});
	const encrypted_key_b64 = base64_encode_bytes(new Uint8Array(encrypted_key_bytes));

	return {
		data_b64: encrypted_key_b64,
		salt_b64: salt_b64,
		algorithm: algorithm,
		iterations: iterations,
		key_type: key_type,
	};
}

async function decrypt_key_with_password(encrypted_key: EncryptedKey, user: User, key_type: EncryptedKeyType, password: string): Promise<CryptoKey> {
	if (encrypted_key.key_type !== key_type) {
		throw new Error("Invalid encrypted key type!")
	}
	const wrap_key = await derive_wrapkey_from_password(password, encrypted_key.salt_b64, encrypted_key.iterations)

	const encrypted_key_bytes: ArrayBuffer = base64_decode_bytes(encrypted_key.data_b64);

	const algorithm = encrypted_key.algorithm;
	if (algorithm.name !== "AES-GCM") {
		throw new Error("Algorithm is not AES-GCM.")
	}
	if (algorithm.tagLength !== 128) {
		throw new Error("Algorithm tagLength must be 128.")
	}

	/**
	 * recreate additionalData, instead of using algorithm.additionalData_b64, as this will also validate if the key is owned by the user.
	 */
	const additionalData = new TextEncoder().encode("A private key owned by " + user.id + " used as " + encrypted_key.key_type);
	const iv = base64_decode_bytes(algorithm.iv_b64);

	const aesgcmparams = { // https://developer.mozilla.org/en-US/docs/Web/API/AesGcmParams
		name: algorithm.name,
		iv: iv,
		additionalData: additionalData,
		tagLength: algorithm.tagLength,
	};
	const extractable = false;

	let key_params: RsaHashedImportParams | EcKeyImportParams | undefined = undefined;
	let usages: Iterable<KeyUsage> | undefined = undefined;
	switch (key_type) {
		case "SIGNATURE_KEY":
			key_params = {
				name: "ECDSA",
				/**
				 * We use P-256 instead of P-384 because it's (supposedly) much faster and still secure enough for us (128-bit security level).
				 * See https://soatok.blog/2022/05/19/guidance-for-choosing-an-elliptic-curve-signature-algorithm-in-2022/
				 */
				namedCurve: "P-256",
			}
			usages = [
				//"verify",
				"sign",
			]
			break;
		case "ENCRYPTION_KEY":
			key_params = {
				name: "RSA-OAEP",
				hash: "SHA-384",
			}
			usages = [
				//"encrypt",
				"decrypt",
				//"wrapKey",
				//"unwrapKey",
			]
			break;
	}

	return await crypto.subtle.unwrapKey(
		"jwk",
		encrypted_key_bytes,
		wrap_key,
		aesgcmparams,
		key_params,
		extractable,
		usages,
	);
}


export class UserKey {
	user: User;
	fingerprint: Fingerprint;

	/**
	 * private key for signing
	 */
	protected signature_key?: CryptoKey;
	protected signature_key_b64_encrypted?: EncryptedKey;
	/**
	 * public key for verifying a signed element
	 */
	protected verification_key: CryptoKey;
	protected verification_key_b64: Base64;

	/**
	 * public key for encryption
	 */
	protected encryption_key: CryptoKey;
	protected encryption_key_b64: Base64;
	protected encryption_key_signature_b64: Base64;

	/**
	 * private key for decryption
	 */
	protected decryption_key?: CryptoKey;
	protected decryption_key_b64_encrypted?: EncryptedKey;

	public static async import(user: User, rawkey: UserKeysResponse<EncryptedKey, EncryptedKey> | UserKeysPubliconlyResponse): Promise<UserKey> {
		if (rawkey.user !== user.id) {
			throw new Error("Cannot create UserKey with key that doesn't belong to user!");
		}
		let signature_key_b64_encrypted = undefined;
		let decryption_key_b64_encrypted = undefined;
		const containsEncryptedPrivateKey = ("decryption_key_b64_encrypted" in rawkey && "signature_key_b64_encrypted" in rawkey);
		if (containsEncryptedPrivateKey && rawkey.signature_key_b64_encrypted && rawkey.decryption_key_b64_encrypted) {
			signature_key_b64_encrypted = rawkey.signature_key_b64_encrypted;
			decryption_key_b64_encrypted = rawkey.decryption_key_b64_encrypted;
		}

		const res = await UserKey.build(
			user,
			rawkey.verification_key_b64,
			rawkey.encryption_key_b64,
			rawkey.encryption_key_signature_b64,
			signature_key_b64_encrypted,
			decryption_key_b64_encrypted,
		)

		// validate fingerprint
		if (res.fingerprint !== rawkey.fingerprint) {
			throw new Error("Provided fingerprint does not match the calculated fingerprint. Someone tempered with the key or the key does not belong to this user! DO NOT TRUST THIS USER!");
		}

		return res;
	}

	public static async generate(user: User, password: string): Promise<UserKey> {
		const encryption_keypair = await crypto.subtle.generateKey(
			{
				name: "RSA-OAEP",
				modulusLength: 4096,
				publicExponent: new Uint8Array([0x01, 0x00, 0x01]), // 65537
				hash: "SHA-384",
			},
			true,
			[
				"encrypt",
				"decrypt",
				//"wrapKey",
				//"unwrapKey",
			]
		);
		const signature_keypair = await crypto.subtle.generateKey(
			{
				name: "ECDSA",
				/**
				 * We use P-256 instead of P-384 because it's (supposedly) much faster and still secure enough for us (128-bit security level).
				 * See https://soatok.blog/2022/05/19/guidance-for-choosing-an-elliptic-curve-signature-algorithm-in-2022/
				 */
				namedCurve: "P-256",
			},
			true,
			[
				"verify",
				"sign",
			]
		);

		const verification_key = signature_keypair.publicKey;
		const verification_jwk = await crypto.subtle.exportKey("jwk", verification_key);
		const verification_key_b64 = base64_encode_json(verification_jwk);

		const encryption_key = encryption_keypair.publicKey;
		const encryption_jwk = await crypto.subtle.exportKey("jwk", encryption_key);
		const encryption_key_b64 = base64_encode_json(encryption_jwk);

		const encryption_key_signature = await crypto.subtle.sign(
			signature_algoritm,
			signature_keypair.privateKey,
			base64_decode_bytes(encryption_key_b64)
		);
		const encryption_key_signature_b64 = base64_encode_bytes(new Uint8Array(encryption_key_signature));

		const signature_key_b64_encrypted = await encrypt_key_with_password(signature_keypair.privateKey, user, "SIGNATURE_KEY", password);
		const decryption_key_b64_encrypted = await encrypt_key_with_password(encryption_keypair.privateKey, user, "ENCRYPTION_KEY", password);

		const res = await UserKey.build(
			user,
			verification_key_b64,
			encryption_key_b64,
			encryption_key_signature_b64,
			signature_key_b64_encrypted,
			decryption_key_b64_encrypted,
		);

		// unlock newly generated key
		// also basically tests if key will be unlockable later
		await res.unlock(password);

		return res;
	}


	private static async build(
		user: User,
		verification_key_b64: Base64,
		encryption_key_b64: Base64,
		encryption_key_signature_b64: Base64,
		signature_key_b64_encrypted?: EncryptedKey,
		decryption_key_b64_encrypted?: EncryptedKey,
	): Promise<UserKey> {
		const encryption_jwk: JsonWebKey = base64_decode_json(encryption_key_b64);
		const encryption_key = await crypto.subtle.importKey(
			"jwk",
			encryption_jwk,
			{
				name: "RSA-OAEP",
				hash: "SHA-384",
			},
			false,
			[
				"encrypt",
				//"wrapKey",
			]
		);

		const verification_jwk: JsonWebKey = base64_decode_json(verification_key_b64);
		const verification_key = await crypto.subtle.importKey(
			"jwk",
			verification_jwk,
			{
				name: "ECDSA",
				/**
				 * We use P-256 instead of P-384 because it's (supposedly) much faster and still secure enough for us (128-bit security level).
				 * See https://soatok.blog/2022/05/19/guidance-for-choosing-an-elliptic-curve-signature-algorithm-in-2022/
				 */
				namedCurve: "P-256",
			},
			false,
			["verify"]
		);

		const fingerprint = await calculate_fingerprint(user, verification_key_b64);

		// validate encryptionkey belongs to signature key
		const encryption_key_valid = await crypto.subtle.verify(
			signature_algoritm,
			verification_key,
			base64_decode_bytes(encryption_key_signature_b64),
			base64_decode_bytes(encryption_key_b64)
		);
		if (!encryption_key_valid) {
			throw new Error("Encryption key was not signed with the signature key! Someone tempered with the key! DO NOT TRUST THIS USER!");
		}

		return (new UserKey(
			user,
			fingerprint,
			verification_key_b64,
			verification_key,
			encryption_key_b64,
			encryption_key,
			encryption_key_signature_b64,
			signature_key_b64_encrypted,
			decryption_key_b64_encrypted,
		));
	}
	private constructor(
		user: User,
		fingerprint: Fingerprint,
		verification_key_b64: Base64,
		verification_key: CryptoKey,
		encryption_key_b64: Base64,
		encryption_key: CryptoKey,
		encryption_key_signature_b64: Base64,
		signature_key_b64_encrypted?: EncryptedKey,
		decryption_key_b64_encrypted?: EncryptedKey,
	) {

		this.user = user;
		this.fingerprint = fingerprint;
		this.verification_key_b64 = verification_key_b64;
		this.verification_key = verification_key;
		this.encryption_key_b64 = encryption_key_b64;
		this.encryption_key = encryption_key;
		this.encryption_key_signature_b64 = encryption_key_signature_b64;
		this.signature_key_b64_encrypted = signature_key_b64_encrypted;
		this.decryption_key_b64_encrypted = decryption_key_b64_encrypted;
	}

	isUnlocked(): boolean {
		return (this.signature_key !== undefined && this.decryption_key !== undefined);
	}

	isUnlockable(): boolean {
		return (this.signature_key_b64_encrypted !== undefined && this.decryption_key_b64_encrypted !== undefined);
	}

	/**
	*
	* @returns this on successful unlocking
	*/
	async unlock(password: string): Promise<UserKey> {
		if (this.isUnlocked()) {
			return this; // already unlocked
		}

		if (this.signature_key_b64_encrypted === undefined || this.decryption_key_b64_encrypted === undefined) {
			throw new Error("Encrypted private keys not available. Is this even your key?");
		}
		const signature_key = await decrypt_key_with_password(this.signature_key_b64_encrypted, this.user, "SIGNATURE_KEY", password);
		const decryption_key = await decrypt_key_with_password(this.decryption_key_b64_encrypted, this.user, "ENCRYPTION_KEY", password);

		// both keys were decrypted.
		this.signature_key = signature_key;
		this.decryption_key = decryption_key;
		return this;
	}

	lock() {
		this.signature_key = undefined;
		this.decryption_key = undefined;
	}


	toUserKeyRecord(): UserKeysRecord<EncryptedKey, EncryptedKey> {
		return {
			"user": this.user.id,
			"encryption_key_b64": this.encryption_key_b64,
			"decryption_key_b64_encrypted": this.decryption_key_b64_encrypted ?? null,
			"verification_key_b64": this.verification_key_b64,
			"signature_key_b64_encrypted": this.signature_key_b64_encrypted ?? null,
			"encryption_key_signature_b64": this.encryption_key_signature_b64,
			"fingerprint": this.fingerprint,
		};
	}

	getId(): string {
		return this.fingerprint.slice(0, 15);
	}

	async encrypt(data_b64: Base64): Promise<EncryptedData> {
		const salt_b64 = base64_encode_bytes(crypto.getRandomValues(new Uint8Array(32)));
		const algorithm: EncryptionAlgorithm = {
			name: "RSA-OAEP",
			label_b64: undefined,
		}
		const payload = {
			// encoding payload using json instead of bytes is kinda slow and resourcewasteful, but it's futureproof.
			"version": 1,
			"data_b64": data_b64,
			"salt_b64": salt_b64,
		}
		const encrypted_data_bytes = await crypto.subtle.encrypt(
			{
				name: algorithm.name,
				label: algorithm.label_b64 ? base64_decode_bytes(algorithm.label_b64) : undefined,
			},
			this.encryption_key,
			base64_decode_bytes(base64_encode_json(payload))
		);
		return {
			data_b64: base64_encode_bytes(new Uint8Array(encrypted_data_bytes)),
			salt_b64: salt_b64,
			algorithm: algorithm,
		}
	}

	async decrypt(data_b64_encrypted: EncryptedData): Promise<Base64> {
		if (!(
			typeof data_b64_encrypted === "object"
			&& "data_b64" in data_b64_encrypted
			&& "salt_b64" in data_b64_encrypted
			&& "algorithm" in data_b64_encrypted
		)) {
			throw new Error("EncryptedData is either missing a field or is not an object.");
		}

		if (!this.decryption_key) {
			throw new Error("Cannot decrypt data. Missing decrypted decryption key.");
		}

		const algorithm = data_b64_encrypted.algorithm;
		if (algorithm.name !== "RSA-OAEP") {
			throw new Error("Algorithm is not RSA-OAEP.")
		}

		const payload_bytes = await crypto.subtle.decrypt(
			{
				name: algorithm.name,
				label: algorithm.label_b64 ? base64_decode_bytes(algorithm.label_b64) : undefined,
			},
			this.decryption_key,
			base64_decode_bytes(data_b64_encrypted.data_b64)
		);
		const payload = base64_decode_json(base64_encode_bytes(new Uint8Array(payload_bytes)));
		if (!("version" in payload && "data_b64" in payload && "salt_b64" in payload)) {
			throw new Error("Cannot decrypt data. Payload malformed.");
		}
		if (payload.version !== 1) {
			throw new Error("Payload is of unknown version.");
		}
		if (payload.salt_b64 !== data_b64_encrypted.salt_b64) {
			throw new Error("Payload salt does not match expected salt! Someone tempered with the encrypted data. DO NOT TRUST THIS USER!");
		}
		if (typeof payload.data_b64 !== "string") {
			throw new Error("Decrypted data is not a string.")
		}
		return payload.data_b64;
	}

	async sign(data_b64: Base64): Promise<Base64> {
		if (!this.signature_key) {
			throw new Error("Cannot sign data. Missing decrypted signature key.");
		}
		const signature = await crypto.subtle.sign(
			signature_algoritm,
			this.signature_key,
			base64_decode_bytes(data_b64)
		);
		return base64_encode_bytes(new Uint8Array(signature));
	}

	async verify(data_b64: Base64, signature_b64: Base64): Promise<boolean> {
		return await crypto.subtle.verify(
			signature_algoritm,
			this.verification_key,
			base64_decode_bytes(signature_b64),
			base64_decode_bytes(data_b64)
		);
	}
}

export class RoomKey {
	room: Room;
	key: CryptoKey;
	constructor(
		room: Room,
		key: CryptoKey,
	) {
		this.room = room;
		this.key = key;
	}

	static async generate(room: Room): Promise<RoomKey> {
		const key = await crypto.subtle.generateKey(
			{
				name: "AES-GCM",
				length: 256,
			},
			true,
			[
				"encrypt",
				"decrypt",
			]
		);
		return new RoomKey(
			room,
			key,
		);
	}

	async encrypt(data_b64: Base64): Promise<EncryptedData> {
		const salt_b64 = base64_encode_bytes(crypto.getRandomValues(new Uint8Array(32)));

		const iv = crypto.getRandomValues(new Uint8Array(32)); // https://nvlpubs.nist.gov/nistpubs/Legacy/SP/nistspecialpublication800-38d.pdf#%5B%7B%22num%22%3A65%2C%22gen%22%3A0%7D%2C%7B%22name%22%3A%22XYZ%22%7D%2C0%2C792%2Cnull%5D
		const additionalData = new TextEncoder().encode("Data encrypted for room " + this.room.id);

		// https://developer.mozilla.org/en-US/docs/Web/API/AesGcmParams
		const algorithm: EncryptionAlgorithm = {
			name: "AES-GCM",
			iv_b64: base64_encode_bytes(iv),
			additionalData_b64: base64_encode_bytes(additionalData),
			tagLength: 128,
		}
		const payload = {
			// encoding payload using json instead of bytes is kinda slow and resourcewasteful, but it's futureproof.
			"version": 1,
			"data_b64": data_b64,
			"salt_b64": salt_b64,
		}
		const encrypted_data_bytes = await crypto.subtle.encrypt(
			{
				name: algorithm.name,
				iv: base64_decode_bytes(algorithm.iv_b64),
				additionalData: base64_decode_bytes(algorithm.additionalData_b64),
				tagLength: algorithm.tagLength,
			},
			this.key,
			base64_decode_bytes(base64_encode_json(payload))
		);
		return {
			data_b64: base64_encode_bytes(new Uint8Array(encrypted_data_bytes)),
			salt_b64: salt_b64,
			algorithm: algorithm,
		}
	}

	async decrypt(data_b64_encrypted: EncryptedData): Promise<Base64> {
		if (!(
			typeof data_b64_encrypted === "object"
			&& "data_b64" in data_b64_encrypted
			&& "salt_b64" in data_b64_encrypted
			&& "algorithm" in data_b64_encrypted
		)) {
			throw new Error("EncryptedData is either missing a field or is not an object.");
		}

		const algorithm = data_b64_encrypted.algorithm;
		const additionalData = new TextEncoder().encode("Data encrypted for room " + this.room.id);
		if (algorithm.name !== "AES-GCM") {
			throw new Error("Algorithm is not AES-GCM.")
		}
		if (algorithm.additionalData_b64 !== base64_encode_bytes(additionalData)) {
			throw new Error("Additional data does not match.")
		}

		const payload_bytes = await crypto.subtle.decrypt(
			{
				name: algorithm.name,
				iv: base64_decode_bytes(algorithm.iv_b64),
				additionalData: additionalData,
				tagLength: algorithm.tagLength,
			},
			this.key,
			base64_decode_bytes(data_b64_encrypted.data_b64)
		);
		const payload = base64_decode_json(base64_encode_bytes(new Uint8Array(payload_bytes)));
		if (!("version" in payload && "data_b64" in payload && "salt_b64" in payload)) {
			throw new Error("Cannot decrypt data. Payload malformed.");
		}
		if (payload.version !== 1) {
			throw new Error("Payload is of unknown version.");
		}
		if (payload.salt_b64 !== data_b64_encrypted.salt_b64) {
			throw new Error("Payload salt does not match expected salt! Someone tempered with the encrypted data. DO NOT TRUST THIS USER!");
		}
		if (typeof payload.data_b64 !== "string") {
			throw new Error("Decrypted data is not a string.")
		}
		return payload.data_b64;
	}

	async symmetric_encrypt_and_sign(signer: UserKey, data_b64: Base64): Promise<SignedEncryptedData> {
		const signature_of_unencrypted_data_b64 = await signer.sign(data_b64);
		const data_encrypted = await this.encrypt(data_b64);
		const signature_of_encrypted_data_b64 = await signer.sign(data_encrypted.data_b64);
		return {
			...data_encrypted,
			signature: {
				fingerprint: signer.fingerprint,
				of_encrypted_data_b64: signature_of_encrypted_data_b64,
				of_unencrypted_data_b64: signature_of_unencrypted_data_b64,
			}
		}
	}

	async symmetric_decrypt_and_verify(signer: UserKey, data_b64_encrypted: SignedEncryptedData): Promise<Base64> {
		if (signer.fingerprint !== data_b64_encrypted.signature.fingerprint) {
			throw new Error("Refusing to decrypt data that was not signed by the expected signer. DO NOT TRUST THIS USER!");
		}
		if (!(await signer.verify(data_b64_encrypted.data_b64, data_b64_encrypted.signature.of_encrypted_data_b64))) {
			throw new Error("Encrypted data was not signed by the expected signer. DO NOT TRUST THIS USER!");
		}
		const data_b64 = await this.decrypt(data_b64_encrypted);
		if (!(await signer.verify(data_b64, data_b64_encrypted.signature.of_unencrypted_data_b64))) {
			throw new Error("Unencrypted data was not signed by the expected signer. DO NOT TRUST THIS USER!");
		}
		return data_b64;
	}

	async symmetric_encrypt_and_sign_json(signer: UserKey, data: object): Promise<SignedEncryptedData> {
		const data_b64 = base64_encode_json(data);
		return await this.symmetric_encrypt_and_sign(signer, data_b64);
	}

	async symmetric_decrypt_and_verify_json(signer: UserKey, data_encrypted: SignedEncryptedData): Promise<object> {
		const data_b64 = await this.symmetric_decrypt_and_verify(signer, data_encrypted);
		return base64_decode_json(data_b64);
	}


}


/* ============================== ASYMMMETRIC ============================== */

export async function asymmetric_encrypt_and_sign(signer: UserKey, target: UserKey, data_b64: Base64): Promise<SignedEncryptedData> {
	const signature_of_unencrypted_data_b64 = await signer.sign(data_b64);
	const data_encrypted = await target.encrypt(data_b64);
	const signature_of_encrypted_data_b64 = await signer.sign(data_encrypted.data_b64);
	return {
		...data_encrypted,
		signature: {
			fingerprint: signer.fingerprint,
			of_encrypted_data_b64: signature_of_encrypted_data_b64,
			of_unencrypted_data_b64: signature_of_unencrypted_data_b64,
		}
	}
}

export async function asymmetric_decrypt_and_verify(signer: UserKey, target: UserKey, data_b64_encrypted: SignedEncryptedData): Promise<Base64> {
	if (signer.fingerprint !== data_b64_encrypted.signature.fingerprint) {
		throw new Error("Refusing to decrypt data that was not signed by the expected signer. DO NOT TRUST THIS USER!");
	}
	if (!(await signer.verify(data_b64_encrypted.data_b64, data_b64_encrypted.signature.of_encrypted_data_b64))) {
		throw new Error("Encrypted data was not signed by the expected signer. DO NOT TRUST THIS USER!");
	}
	const data_b64 = await target.decrypt(data_b64_encrypted);
	if (!(await signer.verify(data_b64, data_b64_encrypted.signature.of_unencrypted_data_b64))) {
		throw new Error("Unencrypted data was not signed by the expected signer. DO NOT TRUST THIS USER!");
	}
	return data_b64;
}

export async function asymmetric_encrypt_and_sign_json(signer: UserKey, target: UserKey, data: object): Promise<SignedEncryptedData> {
	const data_b64 = base64_encode_json(data);
	return await asymmetric_encrypt_and_sign(signer, target, data_b64);
}

export async function asymmetric_decrypt_and_verify_json(signer: UserKey, target: UserKey, data_encrypted: SignedEncryptedData): Promise<object> {
	const data_b64 = await asymmetric_decrypt_and_verify(signer, target, data_encrypted);
	return base64_decode_json(data_b64);
}

export async function asymmetric_encrypt_and_sign_cryptokey(signer: UserKey, target: UserKey, key: CryptoKey): Promise<SignedEncryptedData> {
	const roomkey_jwk = await crypto.subtle.exportKey("jwk", key);
	return await asymmetric_encrypt_and_sign_json(signer, target, roomkey_jwk);
}

export async function asymmetric_decrypt_and_verify_cryptokey(signer: UserKey, target: UserKey, roomkey_encrypted: SignedEncryptedData): Promise<CryptoKey> {
	const roomkey_jwk = await asymmetric_decrypt_and_verify_json(signer, target, roomkey_encrypted);
	return await crypto.subtle.importKey("jwk",
		roomkey_jwk,
		{
			name: "AES-GCM",
			length: 256,
		},
		true,
		[
			"encrypt",
			"decrypt",
		]
	);
}

