import { RoomEventID, RoomEventOrError, Message, RoomStatus, SignedEncryptedRoomEvent, MessageType } from "@/types/room";
import { Collections, RoomEventsResponse, RoomEventsTypeOptions, RoomKeysResponse, RoomsResponse, RoomsTypeOptions } from "@/types/pocketbase-types";
import { asymmetric_decrypt_and_verify_cryptokey, asymmetric_encrypt_and_sign_cryptokey, RoomKey, UserKey } from "./crypto";
import { API, User } from "./api";
import { Form, Group, UserID } from "@/types/api";
import { safeAwait } from "./utils";


export class Room {
	creator: User;
	form: Form;
	member_groups: Group[];
	member_users: Record<UserID, User>;
	subject: string;
	type: RoomsTypeOptions;
	id: string;
	created: string;
	updated: string;
	collectionId: string;
	collectionName: Collections;
	expand?: unknown;

	API: API;

	protected keyscache: RoomKeysResponse[];
	protected eventscache: RoomEvent[];
	protected roomkey?: RoomKey;

	private constructor(
		API: API,
		room: RoomsResponse,
		creator: User,
		form: Form,
		member_groups: Group[],
		member_users: Record<UserID, User>,
	) {
		this.API = API;

		this.creator = creator;
		this.form = form;
		this.member_groups = member_groups;
		this.member_users = member_users;
		this.subject = room.subject;
		this.type = room.type;
		this.id = room.id;
		this.created = room.created;
		this.updated = room.updated;
		this.collectionId = room.collectionId;
		this.collectionName = room.collectionName;
		this.expand = room.expand;

		this.keyscache = [];
		this.eventscache = [];

	}

	static async build(
		API: API,
		room: RoomsResponse,
	): Promise<Room> {
		if (!API.currentuser) {
			throw new Error("Ein Raum Objekt kann nicht erstellt werden, wenn der Nutzer nicht angemeldet ist.");
		}
		const creator = await API.getUser(room.creator);
		const form = await API.getSpecificForm(room.form);
		const member_groups = await API.getGroups(room.member_groups);
		const member_users = await API.getUsers(room.member_users);

		return (new Room(
			API,
			room,
			creator,
			form,
			member_groups,
			member_users,
		));
	}

	async getRecipients(): Promise<Record<UserID, User>> {
		const userids: UserID[] = [...Object.keys(this.member_users)];
		for (const group of this.member_groups) {
			for (const rawuser of group.members) {
				if (!(rawuser.id in userids)) {
					userids.push(rawuser.id);
				}
			}
		}
		return await this.API.getUsers(userids)
	}

	async reshareKeys() {
		const roomkey = await this.getRoomKey();
		if (!this.API.currentuser?.hasUnlockedKey()) {
			throw new Error("User doesn't have an unlocked key.");
		}
		const recipients = await this.getRecipients();
		const userids = Object.keys(recipients);
		const userkeys_records = await Promise.all(userids.map((userid) => {
			// TODO determine with which keys the roomkey has already been shared with
			const user = recipients[userid];
			return user.getUserKeys(false);
		}));
		const userkeys: UserKey[] = userkeys_records.flatMap(record => Object.values(record));
		await Promise.allSettled(userkeys.map(async (target) => {
			const roomkey_encrypted = await asymmetric_encrypt_and_sign_cryptokey(this.API.currentuser!.getUnlockedKey()!, target, roomkey.key);
			const res = await this.API.uploadRoomKey(this, target, roomkey_encrypted);
			return res;
		}));
	}

	private async encrypt_and_sign_roomevent(event: RoomEvent): Promise<SignedEncryptedRoomEvent> {
		const roomkey = await this.getRoomKey();
		if (!this.API.currentuser?.hasUnlockedKey()) {
			throw new Error("Ohne entsperrten Schlüssel kann keine neue Nachricht signiert und verschlüsselt werden.");
		}
		if (this.id !== event.room.id) {
			throw new Error("Refusing to encrypt and sign an event for a different room!");
		}
		const signer = this.API.currentuser.getUnlockedKey()!;
		const data = {
			version: 1,
			id: event.getId(),
			author: event.author.id,
			room: event.room.id,
			unix_timestamp_ms: event.unix_timestamp_ms,
			type: event.type,
			submissionvalue: undefined as undefined | object,
			status: undefined as undefined | RoomStatus,
			message: undefined as undefined | Message,
		};
		if (event instanceof RoomEventSubmissionValue) {
			data.submissionvalue = event.submissionvalue;
		} else if (event instanceof RoomEventStatusChange) {
			data.status = event.status;
		} else if (event instanceof RoomEventMessage) {
			data.message = event.message;
		} else {
			throw new Error("TODO encrypt_and_sign_roomevent cannot handle type " + event.type.toString());
		}
		return await roomkey.symmetric_encrypt_and_sign_json(signer, data);
	}

	private async decrypt_and_verify_roomevent(eventid: RoomEventID, signer: User, event_encrypted: SignedEncryptedRoomEvent): Promise<RoomEvent> {
		const roomkey = await this.getRoomKey();
		const recipients = await this.getRecipients();
		if (!(signer.id in recipients)) {
			throw new Error("Der Nutzer, der diese Nachricht gesendet hat, ist nicht teil dieser Konversation.");
		}
		const userkeys = await signer.getUserKeys(false);
		if (!(event_encrypted.signature.fingerprint in userkeys)) {
			throw new Error("Der Nutzer, der diese Nachricht gesendet hat, besitzt nicht den Schlüssel mit dem die Nachricht signiert wurde. VERTRAUE NICHT DIESEM NUTZER!");
		}
		const data = await roomkey.symmetric_decrypt_and_verify_json(userkeys[event_encrypted.signature.fingerprint], event_encrypted);
		if (!(
			"version" in data && typeof data.version === "number" && data.version === 1
			&& "id" in data && typeof data.id === "string"
			&& "unix_timestamp_ms" in data && typeof data.unix_timestamp_ms === "number"
			&& "room" in data && typeof data.room === "string"
			&& "author" in data && typeof data.author === "string"
			&& "type" in data && typeof data.type === "string"
		)) {
			throw new Error("Event entspricht nicht der korrekten Formattierung.");
		}
		if (data.id !== eventid || RoomEvent.getId(data.unix_timestamp_ms) !== data.id) {
			throw new Error("Event gehört nicht zu dieser EventID! VERTRAUE NICHT DIESEM NUTZER!");
		}

		const type = RoomEventsTypeOptions[data.type as keyof typeof RoomEventsTypeOptions];
		const room = await this.API.getRoom(data.room);
		const author = await this.API.getUser(data.author);
		const unix_timestamp_ms = data.unix_timestamp_ms;

		let res;
		switch (type) {
			case RoomEventsTypeOptions.STATUS_CHANGE: {
				if (!(
					"status" in data && typeof data.status === "string" && Object.values(RoomStatus).includes(data.status as RoomStatus)
				)) {
					throw new Error("Event entspricht nicht der korrekten Formattierung.");
				}
				const status = RoomStatus[data.status as keyof typeof RoomStatus];
				res = new RoomEventStatusChange(author, room, unix_timestamp_ms, status);
				break;
			}
			case RoomEventsTypeOptions.MEMBER_ADD:
				throw new Error("TODO decrypt_and_verify_roomevent cannot handle type " + type.toString());
			case RoomEventsTypeOptions.MESSAGE: {
				if (!(
					"message" in data && typeof data.message === "object" && data.message !== null
					&& "type" in data.message && typeof data.message.type === "string" && Object.values(MessageType).includes(data.message.type as MessageType)
				)) {
					throw new Error("Event entspricht nicht der korrekten Formattierung.");
				}
				switch (MessageType[data.message.type as keyof typeof MessageType]) {
					case (MessageType.TEXT):
						if (!(
							"text" in data.message && typeof data.message.text === "string"
						)) {
							throw new Error("Event entspricht nicht der korrekten Formattierung.");
						}
						break;
					default:
						throw new Error("Nachrichtformat ist unbekannt.");
				};
				const message = data.message as Message;
				res = new RoomEventMessage(author, room, unix_timestamp_ms, message)
				break;
			}
			case RoomEventsTypeOptions.SUBMISSIONVALUE: {
				if (!(
					"submissionvalue" in data && typeof data.submissionvalue === "object"
				)) {
					throw new Error("Event entspricht nicht der korrekten Formattierung.");
				}
				res = new RoomEventSubmissionValue(author, room, unix_timestamp_ms, data.submissionvalue!);
				break;
			}
		}

		if (res.room.id !== this.id) {
			throw new Error("Diese Nachricht gehört nicht in diesen Raum!");
		}

		return res;
	}

	async getEvents(): Promise<RoomEventOrError[]> {
		const rawevents = await this.API.getRoomEvents(this);
		rawevents.sort((a, b) => Date.parse(a.created) - Date.parse(b.created));

		const decrypt_event: ((rawevent: RoomEventsResponse<SignedEncryptedRoomEvent>) => Promise<RoomEventOrError>) = async (rawevent) => {
			const author = await safeAwait<User>(this.API.getUser(rawevent.author));
			if (author.left) {
				return {
					left: {
						id: rawevent.id,
						error: author.left,
						unix_timestamp_ms: Date.parse(rawevent.created),
						room: this, // technisch gesehen wissen wir noch nicht ob es der richtige Raum ist
					}
				};
			}
			// TODO verify that author is/was allowed to post in this room
			if (rawevent.room !== this.id) {
				return {
					left: {
						id: rawevent.id,
						error: new Error("Event ist nicht für diesen Raum."),
						author: author.right,
						unix_timestamp_ms: Date.parse(rawevent.created),
						room: this,
					}
				};
			}
			return await this.decrypt_and_verify_roomevent(rawevent.id, author.right, rawevent.event_encrypted!).then((event) => {
				return { right: event };
			}).catch((error: Error) => {
				return {
					left: {
						id: rawevent.id,
						error: error,
						author: author.right,
						unix_timestamp_ms: Date.parse(rawevent.created),
						room: this,
					}
				};
			});

		};

		const res: RoomEventOrError[] = await Promise.all(rawevents.map(async (rawevent) => await decrypt_event(rawevent)));
		return res;
	}

	private async addEvent(event: RoomEvent): Promise<void> {
		if (event.author.id !== this.API.currentuser?.id) {
			throw new Error("Du darfst keine Events als ein anderer Nutzer erstellen.");
		}
		const event_encrypted = await this.encrypt_and_sign_roomevent(event);
		await this.API.uploadRoomEvent(this, event.type, event_encrypted, event.getId());
		// TODO compare unix_timestamp_ms and resp.created
		this.eventscache.push(event);
	}

	async addEventSubmissionValue(submissionvalue: object): Promise<void> {
		if (!this.API.currentuser) {
			throw new Error("Du musst angemeldet sein um Nachrichten zu erstellen.");
		}
		const event = new RoomEventSubmissionValue(
			this.API.currentuser,
			this,
			Date.now(),
			submissionvalue,
		);
		await this.addEvent(event);
	}

	async addEventStatusChange(status: RoomStatus): Promise<void> {
		if (!this.API.currentuser) {
			throw new Error("Du musst angemeldet sein um Nachrichten zu erstellen.");
		}
		const event = new RoomEventStatusChange(
			this.API.currentuser,
			this,
			Date.now(),
			status,
		);
		await this.addEvent(event);
	}

	async addEventMessage(message: Message): Promise<void> {
		if (!this.API.currentuser) {
			throw new Error("Du musst angemeldet sein um Nachrichten zu erstellen.");
		}
		const event = new RoomEventMessage(
			this.API.currentuser,
			this,
			Date.now(),
			message,
		);
		await this.addEvent(event);
	}

	private async getRoomKey(): Promise<RoomKey> {
		if (this.roomkey) {
			return this.roomkey;
		}

		if (!this.API.currentuser?.hasUnlockedKey()) {
			throw new Error("Ohne entsperrten Schlüssel kann diese Aktion nicht ausgeführt werden.");
		}

		const currentuserkey = this.API.currentuser.getUnlockedKey()!;

		let rawroomkeys = await this.API.getEncryptedRoomKeys(this);
		rawroomkeys = rawroomkeys.filter((rawroomkey) => rawroomkey.target === currentuserkey.getId());
		if (rawroomkeys.length <= 0) {
			throw new Error("Der Raumschlüssel für diese Einreichung wurde noch nicht mit deinem Schlüssel geteilt. Melde dich mit einem anderen Schlüssel an, oder frag eine andere Person in der Einreichung, dass sie sich anmelden soll. Dann wird der Raumschlüssel automatisch mit dir geteilt.");
		}

		const signers = await this.API.getUsers(rawroomkeys.map((rawroomkey) => rawroomkey.source)); // TODO source should be the key of the user that signed

		const roomkey = await Promise.any(rawroomkeys.map(async (rawroomkey) => {
			const signer = signers[rawroomkey.source];
			const signerkeys = await signer.getUserKeys(false);
			return await Promise.any(Object.keys(signerkeys).map(async (fingerprint) => {
				return await asymmetric_decrypt_and_verify_cryptokey(signerkeys[fingerprint], currentuserkey, rawroomkey.roomkey_encrypted!);
			}));
		})).catch(() => {
			throw new Error("Der Raumschlüssel für diese Einreichung wurde noch nicht mit deinem Schlüssel geteilt. Melde dich mit einem anderen Schlüssel an, oder frag eine andere Person in der Einreichung, dass sie sich anmelden soll. Dann wird der Raumschlüssel automatisch mit dir geteilt.");
		});
		this.roomkey = new RoomKey(this, roomkey);
		return this.roomkey;
	}

	setRoomKey(roomkey: RoomKey) {
		this.roomkey = roomkey;
	}

}

export abstract class RoomEvent {
	author: User;
	room: Room;
	unix_timestamp_ms: number;
	type: RoomEventsTypeOptions;

	constructor(
		author: User,
		room: Room,
		unix_timestamp_ms: number,
		type: RoomEventsTypeOptions,
	) {
		this.author = author;
		this.room = room;
		this.unix_timestamp_ms = unix_timestamp_ms;
		this.type = type;
	}

	getId(): RoomEventID {
		return RoomEvent.getId(this.unix_timestamp_ms);
	}

	static getId(unix_timestamp_ms: number): RoomEventID {
		return unix_timestamp_ms.toString().padStart(15, "0");
	}

}

export class RoomEventSubmissionValue extends RoomEvent {
	submissionvalue: object;

	constructor(
		author: User,
		room: Room,
		unix_timestamp_ms: number,
		submissionvalue: object,
	) {
		super(
			author,
			room,
			unix_timestamp_ms,
			RoomEventsTypeOptions.SUBMISSIONVALUE,
		);
		this.submissionvalue = submissionvalue;
	}
}

export class RoomEventStatusChange extends RoomEvent {
	status: RoomStatus;

	constructor(
		author: User,
		room: Room,
		unix_timestamp_ms: number,
		status: RoomStatus,
	) {
		super(
			author,
			room,
			unix_timestamp_ms,
			RoomEventsTypeOptions.STATUS_CHANGE,
		);
		this.status = status;
	}
}

export class RoomEventMessage extends RoomEvent {
	message: Message;

	constructor(
		author: User,
		room: Room,
		unix_timestamp_ms: number,
		message: Message
	) {
		super(
			author,
			room,
			unix_timestamp_ms,
			RoomEventsTypeOptions.MESSAGE,
		);
		this.message = message;
	}
}
