import type { Either } from "@/types/utils"
import { type ClassValue, clsx } from "clsx"
import { twMerge } from "tailwind-merge"

export function cn(...inputs: ClassValue[]) {
	return twMerge(clsx(inputs))
}

export async function safeAwait<T>(promise: Promise<T>): Promise<Either<Error, T>> {
	return await promise.then((v) => {
		return {
			right: v,
		};
	}).catch((error: Error) => {
		return {
			left: error,
		};
	});
}

export function formatRelativeTime(milliseconds: number | Date, isrelative = false): string {
	if (milliseconds instanceof Date) {
		milliseconds = milliseconds.getTime();
	}
	if (!isrelative) {
		return formatRelativeTime(milliseconds - Date.now(), true);
	}
	const isFuture = milliseconds > 0;
	milliseconds = Math.abs(milliseconds);

	let seconds = (milliseconds / 1000);
	let minutes = (seconds / 60);
	let hours = (minutes / 60);
	let days = (hours / 24);

	seconds = Math.floor(seconds);
	minutes = Math.floor(minutes);
	hours = Math.floor(hours);
	days = Math.floor(days);


	const rtf = new Intl.RelativeTimeFormat('de', {
		numeric: isFuture ? 'auto' : 'always',
	});
	if (seconds < 60) {
		return rtf.format(isFuture ? seconds : -seconds, 'second');
	} else if (minutes < 60) {
		return rtf.format(isFuture ? minutes : -minutes, 'minute');
	} else if (hours < 24) {
		return rtf.format(isFuture ? hours : -hours, 'hour');
	} else {
		return rtf.format(isFuture ? days : -days, 'day');
	}
}


