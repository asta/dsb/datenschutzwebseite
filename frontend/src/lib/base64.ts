
/**
 * @file
 * Functions for encoding and decoding base64.
 *
 * @see https://developer.mozilla.org/en-US/docs/Glossary/Base64
 */

import type { Base64, Base64JSON } from "@/types/base64";

/**
 * Convert bytes to a base64 encoded string.
 * @param {Uint8Array} bytes The bytes to encode
 * @returns {Base64} The base64 string.
 */
function base64_encode_bytes(bytes: Uint8Array): Base64 {
	const binString = String.fromCodePoint(...bytes);
	const base64 = btoa(binString);
	return base64;
}

/**
 * Convert a base64 encoded string, that was created via base64_encode_bytes back to the bytes.
 * @param {Base64} base64 The base64 string to decode.
 * @returns {Uint8Array} The decoded bytes.
 */
function base64_decode_bytes(base64: Base64): Uint8Array {
	const binString = atob(base64);
	return Uint8Array.from(binString, (m) => m.codePointAt(0)!);
}


/**
 * Convert a JS object to a base64 encoded string.
 * @param {object} value The object to encode.
 * @returns {Base64JSON} The base64 string.
 */
function base64_encode_json(value: object): Base64JSON {
	if (typeof value !== "object") {
		throw new Error("base64_encode_json can only encode objects!")
	}
	const jsonString = JSON.stringify(value);
	const jsonBytes = new TextEncoder().encode(jsonString);
	return base64_encode_bytes(jsonBytes);
}

/**
 * Convert a base64 encoded string, that was created via base64_encode_json to a JS object.
 * @param {Base64JSON} base64 The base64 string to decode.
 * @returns {object} The decoded object.
 */
function base64_decode_json(base64: Base64JSON): object {
	const cleaner = (key: string, value: unknown) => key === "__proto__" ? undefined : value; // https://stackoverflow.com/questions/63926663/how-should-untrusted-json-be-sanitized-before-using-json-parse

	const jsonBytes = base64_decode_bytes(base64);
	const jsonString = new TextDecoder().decode(jsonBytes);
	const value = JSON.parse(jsonString, cleaner) as object;
	if (typeof value !== "object") {
		throw new Error("Failed to decode base64 encoded json object, because it isn't an object. This indicates either a malicious attack or an incompetent programmer.");
	}
	return value;
}

export {
	base64_encode_bytes, base64_decode_bytes,
	base64_encode_json, base64_decode_json,
};


